<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'uk_UA',
    'bootstrap' => ['log'],
    'name' => 'CubeWood',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'layout' => 'main',
            'defaultRoute' => '/products',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableRegistration' => false,
            'enableConfirmation' => false,
            'enablePasswordRecovery' => false,
            'urlPrefix' => '',
            'modelMap' => [
                'User' => 'app\models\User',
            ],
            'controllerMap' => [
                'security' => 'app\controllers\SecurityController',
                'registration' => 'app\controllers\RegistrationController'
            ],
            'admins' => ['admin'],
        ],
        'robotsTxt' => [
            'class' => 'execut\robotsTxt\Module',
            'components' => [
                'generator' => [
                    'class' => \execut\robotsTxt\Generator::className(),
                    'sitemap' => 'sitemap.xml',
                    'userAgent' => [
                        '*' => [
                            'Disallow' => [
                                '/admin/*',
                                '/site/prices',
                                'prices',
                            ],
                            'Allow' => [
                                '/',
                                '/product/',
                                '/aboutus',
                                '/payment-and-delivery',
                                '/testimonials'
                            ],
                        ],
                        'BingBot' => [
                            'Sitemap' => '/sitemapSpecialForBing.xml',
                            'Disallow' => [
                                '/admin/*',
                                '/site/prices',
                                'prices',
                            ],
                            'Allow' => [
                                '/',
                                '/product/',
                                '/aboutus',
                                '/payment-and-delivery',
                                '/testimonials'
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user'
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ZtNDzog4N48FCpIIGBeIsZB1UtLZ8EAH',
            'baseUrl' => '',
        ],

        'user' => [
            'identityClass' => 'dektrium\user\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 's12.thehost.com.ua',
                'username' => 'cubewood@cubewood.com.ua',
                'password' => 'bublyk1984',
                'port' => '587',
                'encryption' => 'tls',

            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ['pattern' => 'robots', 'route' => 'robotsTxt/web/index', 'suffix' => '.txt'],
                'sitemap.xml' => 'site/sitemap',
                '' => 'site/index',
                '/' => 'site/index',
                '/admin' => '/admin',
                'payment-and-delivery' => 'site/payment-and-delivery',
                'testimonials' => 'site/testimonials',
                'image-crop' => 'site/image-crop',
                'count-control' => 'site/count-control',
                'aboutus' => 'site/aboutus',
                'prices' => 'site/prices',
                'search' => 'site/search',
                'subscribe' => 'subscribe/subscribe',
                'unsubscribe' => 'subscribe/unsubscribe',
                'cart' => 'site/cart',
                'cart/buy' => 'site/cart-buy',
                'cart-change-color' => 'site/cart-change-color',
                'error503' => 'site/error503',
                'cart-add' => 'site/cart-add',
                'cart-remove' => 'site/cart-remove',
                'product/<url:[\w-]+>' => 'site/product',
                'login' => '/user/security/login',
                'register' => '/user/registration/register',
                'logout' => '/user/security/logout',
                '/<url:[\w-]+>' => '/',
                'site/<action:[\w-]+>' => 'site/error',
                '<controller:[\w-]+>/' => 'site/error',
                'site/<url:(.*)>' => 'site/error',
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['93.178.222.242'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['93.178.222.242'],
    ];
}

return $config;
