<?php
/**
 * Created by PhpStorm.
 * User: Athlon
 * Date: 25.08.2018
 * Time: 15:33
 */

namespace app\controllers;


use app\models\Category;

class RegistrationController extends \dektrium\user\controllers\RegistrationController
{
    public function beforeAction($action)
    {
        $this->view->params['categorys'] = Category::find()->all();

        return parent::beforeAction($action);
    }
}