<?php

namespace app\controllers;

use app\models\Category;

class SecurityController extends \dektrium\user\controllers\SecurityController
{
    public function beforeAction($action)
    {
        $this->view->params['categorys'] = Category::find()->all();

        return parent::beforeAction($action);
    }
}