<?php

namespace app\controllers;

use app\models\Category;
use app\models\Coefficients;
use app\models\PaymentAndDeliveryForm;
use app\models\Products;
use app\models\PurchaseForm;
use app\models\Testimonials;
use Yii;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sitemap;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'cart-add' => ['post'],
                    'cart-remove' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->view->params['categorys'] = Category::find()->orderBy('id')->all();
        $cart = $this->getCart();
        $this->view->params['cart'] = $cart;
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($url = null, $search = null)
    {
        $notfound = false;
        if ($search != null) {
            $url = null;
        }
        if ($url == null) {
            if ($search == null) {
                $products = Products::find()->orderBy('position')->all();
            } else {
                $products = Products::find()->filterWhere(['like', 'name', $search])->all();
            }
        } else {
            $category = Category::find()->where(['url' => $url])->select('id')->one();
            $products = Products::find()->where(['category_id' => $category->id])->all();
            if ($category == null) {
                throw new NotFoundHttpException(Yii::t('app', 'Сторінку не знайдено!'));
            } else if($products == null){
                $notfound = true;
            }
        }
        if ($products != null) {
            foreach ($products as $product) {
                $colors = glob('uploads/' . $product->imageFolder . '/*');
                foreach ($colors as $color) {
                    $files = glob($color . '/*');
                    foreach ($files as $file) {
                        $product->images[] = Html::img('@web/' . $file, ['alt' => $product->name, 'srcset' => '/' . $product->imageCrop($file, 450, 450, $product->url) . ' 2050w, /' . $product->imageCrop($file, 350, 350, $product->url) . ' 1440w, /' . $product->imageCrop($file, 300, 300, $product->url) . ' 320w']);
                    }
                }
                if (empty($product->images)) {
                    $product->images[] = Html::img('@web/img/no_image.png', ['alt' => 'no-image']);
                }
            }
        } else {
            $notfound = true;
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('index', ['products' => $products, 'notfound' => $notfound]);
        }
        $testimonials = Testimonials::find()->with(['product'])->where(['active' => 1])->limit(2)->orderBy(new Expression('rand()'))->all();
        foreach ($testimonials as $testimonial) {
            $colors = glob('uploads/' . $testimonial->product->imageFolder . '/*');
            foreach ($colors as $color) {
                $files = glob($color . '/*');
                foreach ($files as $file) {
                    $testimonial->product->images[] = Html::img('@web/' . $file, ['alt' => $testimonial->product->name]);
                }
            }
        }
        setlocale(LC_ALL, 'uk_UA.utf8');
        $this->view->registerMetaTag(['name' => 'description', 'content' => 'Сubewood – це сучасний предметний дизайн створений з дерева і натхненний природою.']);
        Yii::$app->response->statusCode = 200;
        return $this->render('index', ['products' => $products, 'testimonials' => $testimonials, 'notfound' => $notfound]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionProduct($url, $color = null)
    {
        if ($color == null) {
            $color = Yii::$app->request->post('color');
            $color = str_replace('#', '', $color);
        }
        $model = Products::find()->where(['url' => $url])->one();
        if ($model === null) {
            throw new NotFoundHttpException(Yii::t('app', 'Сторінку не знайдено!'));
        }
        $images = [];
        if ($color == null) {
            $images = $this->getAllImages($model->imageFolder);
        } else {
            if (file_exists('uploads/' . $model->imageFolder . '/' . $color . '/')) {
                $files = glob('uploads/' . $model->imageFolder . '/' . $color . '/*');
                foreach ($files as $file) {
                    $images[] = Html::img('@web/' . $file);
                }
            } else {
                $images = $this->getAllImages($model->imageFolder);
            }
        }
        $purchaseForm = new PurchaseForm();
        $purchaseForm->scenario = 'one';
        if ($purchaseForm->load(Yii::$app->request->post())) {
            if ($purchaseForm->contact()) {
                Yii::$app->session->setFlash('message', Yii::t('app', 'Дякуємо за замовлення. Скоро з вами зв\'яжуться для подальших вказівок'));
                $this->redirect('/');
            }
        }
        $model->loadModel();
        $this->view->registerMetaTag(['name' => 'description', 'content' => $model->name.' - '. $model->detalization . ' ' . strip_tags($model->description)]);
        return $this->render('product', ['model' => $model, 'images' => $images, 'purchaseForm' => $purchaseForm]);
    }

    private function getAllImages($imageFolder)
    {
        $colors = glob('uploads/' . $imageFolder . '/*');
        foreach ($colors as $color) {
            $files = glob($color . '/*');
            foreach ($files as $file) {
                $images[] = Html::img('@web/' . $file);
            }
        }
        if (empty($images)) {
            $images[] = Html::img('@web/img/no_image.png');
        }
        return $images;
    }

    public function actionPaymentAndDelivery()
    {
        $paymentanddelivery = new PaymentAndDeliveryForm();
        if ($paymentanddelivery->load(Yii::$app->request->post())) {
            if ($paymentanddelivery->contact()) {
                Yii::$app->session->setFlash('message', Yii::t('app', 'Дякуємо за запитання. Скоро з вами зв\'яжуться'));
                $this->redirect('/');
            }
        }
        Yii::$app->response->statusCode = 200;
        return $this->render('paymentanddelivery', ['paymentanddelivery' => $paymentanddelivery]);
    }

    public function actionTestimonials()
    {
        setlocale(LC_ALL, 'uk_UA.utf8');
        $testimonials = Testimonials::find()->with(['product'])->where(['active' => 1])->all();
        foreach ($testimonials as $testimonial) {
            $colors = glob('uploads/' . $testimonial->product->imageFolder . '/*');
            foreach ($colors as $color) {
                $files = glob($color . '/*');
                foreach ($files as $file) {
                    $testimonial->product->images[] = Html::img('@web/' . $file);
                }
            }
        }
        return $this->render('testimonials', ['testimonials' => $testimonials]);
    }

    public function actionAboutus()
    {
        Yii::$app->response->statusCode = 200;
        return $this->render('aboutus');
    }

    public function actionCart()
    {
        Yii::$app->response->statusCode = 200;
        return $this->render('cart', ['items' => $this->getCart()]);
    }

    public function actionCartBuy(){
        $items = $this->getCart();
        $purchaseForm = new PurchaseForm();
        if(count($items) > 1){
            $purchaseForm->scenario = 'cart';
        } else {
            $purchaseForm->scenario = 'one';
        }
        if ($purchaseForm->load(Yii::$app->request->post())) {
                if ($purchaseForm->buyCart()) {
                    $this->clearCart();
                    Yii::$app->session->setFlash('message', Yii::t('app', 'Дякуємо за замовлення. Скоро з вами зв\'яжуться для подальших вказівок'));
                    $this->redirect('/');
                }
        }
        return $this->render('cart-buy', ['items' => $items, 'purchaseForm' => $purchaseForm]);
    }

    public function actionCartAdd()
    {
        $new_cookies = [];
        $id = Yii::$app->request->post('id');
        $color = Yii::$app->request->post('color');
        $count = Yii::$app->request->post('count');
        $cookies = Yii::$app->request->cookies;
        if ($cookies->has('cart')) {
            foreach ($cookies['cart']->value as $key => $cookie) {
                $new_cookies[$key] = $cookie;
            }
        }

        $prodArray = ['color' => $color, 'count' => $count];
        $new_cookies[$id] = $prodArray;
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new Cookie([
            'name' => 'cart',
            'value' => $new_cookies,
        ]));
        return $this->asJson($new_cookies);
    }
    private function clearCart(){
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new Cookie([
            'name' => 'cart',
            'value' => [],
        ]));
    }
    public function actionCartRemove()
    {
        $new_cookies = [];
        $id = Yii::$app->request->post('id');
        $cookies = Yii::$app->request->cookies;
        if ($cookies->has('cart')) {
            foreach ($cookies['cart']->value as $cookie) {
                array_push($new_cookies, $cookie);
            }
            unset($new_cookies[array_search($id, $new_cookies)]);
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new Cookie([
                'name' => 'cart',
                'value' => $new_cookies,
            ]));
        }
        return $this->asJson($new_cookies);
    }

    public function actionError503()
    {

        return $this->render('error', ['message' => Yii::t('app', 'Вибачте, але сервер перегружений')]);
    }

    private function getCart()
    {
        $cart = [];
        $cookies = Yii::$app->request->cookies;
        if ($cookies->has('cart')) {
            foreach ($cookies['cart']->value as $key => $cookie) {
                $product = Products::findOne($key);
                if ($product != null) {
                    $product->count = $cookie['count'];
                    $cart[] = $product;
                    $colors = glob('uploads/' . $product->imageFolder . '/*');
                    $product->selColor = $cookie['color'];
                    foreach ($colors as $color) {
                        $files = glob($color . '/*');
                        foreach ($files as $file) {
                            $product->images = '/' . $product->imageCrop($file,100,100,$product->url);
                            break;
                        }
                        break;
                    }
                    if (empty($product->images)) {
                        $product->images = "/img/no_image.png";
                    }
                }
            }
        }
        return $cart;
    }

    public function actionCountControl()
    {
        $fuc = Yii::$app->request->post('function');
        $id = Yii::$app->request->post('id');
        if ($fuc == "+") {
            $cookies = Yii::$app->request->cookies;
            if ($cookies->has('cart')) {
                $arr = $cookies['cart']->value;
                $arr[$id]['count'] += 1;
                $cookies = Yii::$app->response->cookies;
                $cookies->add(new Cookie([
                    'name' => 'cart',
                    'value' => $arr,
                ]));
            }
        } elseif ($fuc == "-") {
            $cookies = Yii::$app->request->cookies;
            if ($cookies->has('cart')) {
                $arr = $cookies['cart']->value;
                $arr[$id]['count'] -= 1;
                $cookies = Yii::$app->response->cookies;
                $cookies->add(new Cookie([
                    'name' => 'cart',
                    'value' => $arr,
                ]));
            }
        } elseif ($fuc == "=") {
            $count = Yii::$app->request->post('count');
            $cookies = Yii::$app->request->cookies;
            if ($cookies->has('cart')) {
                $arr = $cookies['cart']->value;
                $arr[$id]['count'] = $count;
                $cookies = Yii::$app->response->cookies;
                $cookies->add(new Cookie([
                    'name' => 'cart',
                    'value' => $arr,
                ]));
            }
        }

    }

    public function actionCartChangeColor(){
        $id = Yii::$app->request->post('id');
        $color = Yii::$app->request->post('color');
        $cookies = Yii::$app->request->cookies;
        if ($cookies->has('cart')) {
            $arr = $cookies['cart']->value;
            $arr[$id]['color'] = $color;
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new Cookie([
                'name' => 'cart',
                'value' => $arr,
            ]));
        }
    }

    public function actionPrices()
    {
        $products = Products::find()->all();
        if ($products != null) {
            foreach ($products as $product) {
                $colors = glob('uploads/' . $product->imageFolder . '/*');
                foreach ($colors as $color) {
                    $files = glob($color . '/*');
                    foreach ($files as $file) {
                        $product->images[] = Html::img('@web/' . $product->imageCrop($file, 200, 200, $product->url));
                    }
                }
                if (empty($product->images)) {
                    $product->images[] = Html::img('@web/img/no_image.png');
                }
            }
        }
        $coefficients = Coefficients::findOne(1);
        return $this->render('prices', ['products' => $products, 'coefficients' => $coefficients]);
    }

    public function actionSitemap()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'image/svg+xml');
        return $this->renderPartial('sitemap.xml');
    }

}
