<?php
namespace app\controllers;


use app\models\Subscribe;
use yii\web\Controller;
use Yii;

class SubscribeController extends Controller
{
    function actionSubscribe(){
        $email = Yii::$app->request->post('email');
        $subscribe = new Subscribe();
        $subscribe->email = $email;
        $subscribe->token = md5($email.time().'ok');
        if ($subscribe->save()){
            return $this->asJson(['type'=>'success', 'message' => Yii::t('app', 'Дякуємо за підписку')]);
        } else {
            return $this->asJson(['type'=>'error', 'message' => $subscribe->getFirstError('email')]);
        }
    }

    function actionUnsubscribe($token){

    }
}