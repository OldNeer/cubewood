<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180823_104840_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'url' => $this->string(255)->notNull()->unique(),
            'price' => $this->double()->notNull(),
            'colors' => $this->string(255),
            'discount' => $this->integer(),
            'description' => $this->text(),
            'category_id' => $this->integer(255)->notNull(),
            'imageFolder' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product');
    }
}
