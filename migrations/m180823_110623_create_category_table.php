<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m180823_110623_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string('255'),
        ]);
        $this->insert('{{%category}}',['name' => 'Підвісні']);
        $this->insert('{{%category}}',['name' => 'Настільні']);
        $this->insert('{{%category}}',['name' => 'Бра']);
        $this->insert('{{%category}}',['name' => 'Торшери']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }
}
