<?php

use yii\db\Migration;

/**
 * Class m180823_110953_addforeigenkey_to_product_table
 */
class m180823_110953_addforeigenkey_to_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-category_id',
            '{{%product}}',
            'category_id',
            '{{%category}}',
            'id',
            'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-category_id','{{%product}}');
    }

}
