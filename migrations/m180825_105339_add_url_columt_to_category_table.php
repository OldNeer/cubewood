<?php

use yii\db\Migration;

/**
 * Class m180825_105339_add_url_columt_to_category_table
 */
class m180825_105339_add_url_columt_to_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'url', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%category}}', 'url');
    }

}
