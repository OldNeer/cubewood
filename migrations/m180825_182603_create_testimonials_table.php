<?php

use yii\db\Migration;

/**
 * Handles the creation of table `testimonials`.
 */
class m180825_182603_create_testimonials_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%testimonials}}', [
            'id' => $this->primaryKey(),
            'text' => $this->text()->notNull(),
            'username' => $this->string('255'),
            'userurl' => $this->string('255'),
            'date' => $this->date()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'active' => $this->boolean()->defaultValue(0),
        ]);
        $this->addForeignKey('fk-testimonials_product', '{{%testimonials}}', 'product_id', '{{%product}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-testimonials_product', '{{%testimonials}}');
        $this->dropTable('testimonials');
    }
}
