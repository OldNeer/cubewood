<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subscribe`.
 */
class m180830_075537_create_subscribe_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('subscribe', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255)->notNull(),
            'token' => $this->string(32)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('subscribe');
    }
}
