<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 *
 * @property Product[] $products
 */
class Category extends \yii\db\ActiveRecord
{
    public $parent_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'url', 'parent_name'], 'string', 'max' => 255],
            [['is_label', 'in_nav'], 'boolean', 'trueValue' => 1, 'falseValue' => 0],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => self::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Назва'),
            'is_label' => Yii::t('app', 'Заголовок'),
            'parent_id' => Yii::t('app', 'Батько'),
            'in_nav' => Yii::t('app', 'У навігаційному меню')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

    public function getParent(){
        return$this->hasOne(self::className(), ['id' => 'parent_id']);
    }
}
