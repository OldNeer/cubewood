<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%characteristic}}".
 *
 * @property int $id
 * @property int $publication_id
 * @property string $name
 * @property string $text
 *
 * @property Product $publication
 */
class Characteristic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%characteristic}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['publication_id', 'name', 'text'], 'required'],
            [['publication_id'], 'integer'],
            [['name', 'text'], 'string', 'max' => 255],
            [['publication_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['publication_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'publication_id' => Yii::t('app', 'Publication ID'),
            'name' => Yii::t('app', 'Name'),
            'text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublication()
    {
        return $this->hasOne(Product::className(), ['id' => 'publication_id']);
    }
}
