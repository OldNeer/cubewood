<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%coefficients}}".
 *
 * @property int $id
 * @property string $dropshipping
 * @property string $wholesale
 */
class Coefficients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%coefficients}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dropshipping', 'wholesale'], 'required'],
            [['dropshipping', 'wholesale','wholesale50'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dropshipping' => Yii::t('app', 'Дропшипінг'),
            'wholesale' => Yii::t('app', 'Опт'),
            'wholesale 50' => Yii::t('app', 'Опт 50'),
        ];
    }
}
