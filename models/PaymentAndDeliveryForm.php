<?php

namespace app\models;

use Yii;
use yii\base\Model;

class PaymentAndDeliveryForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $text;

    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'text'], 'required'],
            [['name', 'phone'], 'string', 'max' => 255],
            ['email', 'email'],
            ['text', 'safe']
        ];
    }
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Ім\'я'),
            'phone' => Yii::t('app', 'Телефон'),
            'text' => Yii::t('app', 'Повідомлення')
        ];
    }

    public function contact(){
            if ($this->validate()) {
                Yii::$app->mailer->compose()
                    ->setTo('cubewood.nature@gmail.com')
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setSubject('Запитання')
                    ->setHtmlBody('<p>Ім\'я: '.$this->name.'</p><p>Телефон: '.$this->phone.'</p><p>Email: '.$this->email.'</p><p>Текст: </p><p>'.$this->text.'</p><hr>')
                    ->send();
                return true;
            }
            return false;
    }
}