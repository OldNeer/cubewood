<?php

namespace app\models;

use Yii;
use app\models\Category;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property double $price
 * @property string $colors
 * @property int $discount
 * @property string $description
 * @property int $category_id
 *
 * @property Category $category
 */
class Products extends \yii\db\ActiveRecord
{
    public $images = [];
    public $count = 1;
    public $selColor;
    public $sort = false;
    public $characteristicName;
    public $characteristicText;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url', 'price', 'category_id'], 'required'],
            [['price'], 'number'],
            [['category_id', 'position'], 'integer'],
            [['enableColor'], 'integer', 'min' => 0, 'max' => 1],
            ['discount', 'integer', 'min' => 0, 'max' => 100],
            [['description', 'priceDesc'], 'string'],
            [['detalization', 'name', 'url', 'colors', 'imageFolder', 'selColor'], 'string', 'max' => 255],
            [['url'], 'unique'],
            [['characteristicName', 'characteristicText'], 'each', 'rule' => ['string', 'max' => 255]],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Назва'),
            'url' => Yii::t('app', 'Силка'),
            'price' => Yii::t('app', 'Ціна'),
            'colors' => Yii::t('app', 'Кольори'),
            'discount' => Yii::t('app', 'Знижка %'),
            'description' => Yii::t('app', 'Описання'),
            'category_id' => Yii::t('app', 'Категорія'),
            'priceDesc' => Yii::t('app', 'Примітка'),
            'enableColor' => Yii::t('app', 'Відображення кольорів'),
            'detalization' => Yii::t('app', 'Деталізація'),
            'characteristicText' => Yii::t('app', 'Текст'),
            'characteristicName' => Yii::t('app', 'Назва'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristics()
    {
        return $this->hasMany(Characteristic::className(), ['publication_id' => 'id']);
    }

    public function loadModel()
    {
        $model = Characteristic::find()->where(['publication_id' => $this->id])->all();
        $this->characteristicName = ArrayHelper::map($model, 'id', 'name');
        $this->characteristicText = ArrayHelper::map($model, 'id', 'text');
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        if (!$this->sort) {
            if (count($this->characteristicName) > 0) {
                Characteristic::deleteAll(['publication_id' => $this->id]);
                foreach ($this->characteristicName as $k => $v) {
                    $model = new Characteristic();
                    $model->publication_id = $this->id;
                    $model->name = $v;
                    $model->text = $this->characteristicText[$k];
                    $model->save();
                }
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        Characteristic::deleteAll(['publication_id' => $this->id]);
        return parent::beforeDelete();
    }


    /**
     * @param $image
     * @param $w
     * @param $h
     * @param $name
     * @return string
     */
    public function imageCrop($image, $w, $h, $name)
    {
        header('Cache-Control: public, max-age=3600');
        if (file_exists('cache/' . $name . '/' . md5($image . $w . $h) . '.jpeg')) {
            return 'cache/' . $name . '/' . md5($image . $w . $h) . '.jpeg';
        }
        if (($w < 0) || ($h < 0)) {
            return false;
        }
        $lAllowedExtensions = array(1 => "jpg", 2 => "jpeg", 3 => "png");
        list($lInitialImageWidth, $lInitialImageHeight, $lImageExtensionId) = getimagesize($image);

        if (!array_key_exists($lImageExtensionId, $lAllowedExtensions)) {
            return false;
        }
        $lImageExtension = $lAllowedExtensions[$lImageExtensionId];
        $func = 'imagecreatefrom' . $lImageExtension;
        $lInitialImageDescriptor = $func($image);
        $lCroppedImageWidth = 0;
        $lCroppedImageHeight = 0;
        $lInitialImageCroppingX = 0;
        $lInitialImageCroppingY = 0;
        if ($w / $h > $lInitialImageWidth / $lInitialImageHeight) {
            $lCroppedImageWidth = floor($lInitialImageWidth);
            $lCroppedImageHeight = floor($lInitialImageWidth * $h / $w);
            $lInitialImageCroppingY = floor(($lInitialImageHeight - $lCroppedImageHeight) / 2);
        } else {
            $lCroppedImageWidth = floor($lInitialImageHeight * $w / $h);
            $lCroppedImageHeight = floor($lInitialImageHeight);
            $lInitialImageCroppingX = floor(($lInitialImageWidth - $lCroppedImageWidth) / 2);
        }
        $lNewImageDescriptor = imagecreatetruecolor($w, $h);
        imagecopyresampled($lNewImageDescriptor, $lInitialImageDescriptor, 0, 0, $lInitialImageCroppingX, $lInitialImageCroppingY, $w, $h, $lCroppedImageWidth, $lCroppedImageHeight);
        if (!file_exists('cache/' . $name)) {
            mkdir('cache/' . $name);
        }
        imagejpeg($lNewImageDescriptor, 'cache/' . $name . '/' . md5($image . $w . $h) . '.' . $lImageExtension);
        imagedestroy($lNewImageDescriptor);
        return 'cache/' . $name . '/' . md5($image . $w . $h) . '.' . $lImageExtension;
    }
}
