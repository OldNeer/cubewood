<?php

namespace app\models;

use Yii;
use yii\base\Model;

class PurchaseForm extends Model
{
    public $product;
    public $name;
    public $count;
    public $email;
    public $phone;
    public $notes;
    public $city;
    public $address;
    public $color;

    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'city', 'address', 'count', 'product'], 'required'],
            [['name', 'phone'], 'string', 'max' => 255],
            [['color', 'product'], 'each', 'rule' => ['string', 'max' => 255], 'on' => 'cart'],
            ['count', 'each', 'rule' => ['integer', 'min' => 1, 'max' => 10], 'on' => 'cart'],
            [['color', 'product'], 'string', 'max' => 255, 'on' => 'one'],
            ['count', 'integer', 'min' => 1, 'max' => 10, 'on' => 'one'],
            ['email', 'email'],
            ['notes', 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'ПІБ'),
            'phone' => Yii::t('app', 'Телефон'),
            'notes' => Yii::t('app', 'Примітка'),
            'country' => Yii::t('app', 'Країна'),
            'region' => Yii::t('app', 'Область'),
            'city' => Yii::t('app', 'Населений пункт'),
            'address' => Yii::t('app', 'Адреса'),
            'count' => Yii::t('app', 'Кількість'),
            'color' => Yii::t('app', 'Колір шнура'),
        ];
    }

    public function contact()
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo('cubewood.nature@gmail.com')
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setSubject('Замовлення')
                ->setHtmlBody('
                <p>Товар: ' . $this->product . '</p>
                <p>ПІБ: ' . $this->name . '</p>
                <p>Телефон: ' . $this->phone . '</p>
                <p>Email: ' . $this->email . '</p>
                <p>Населений пункт: ' . $this->city . '</p>
                <p>Адрес: ' . $this->address . '</p>
                <div>Колір: <div style="width: 25px; height: 25px; display: inline-block; background-color: '.$this->color.';"></div></div>
                <p>Кількість: ' . $this->count . '</p>
                <p>Нотатка: </p><p>' . $this->notes . '</p>
                <hr>')
                ->send();
            return true;
        }
        return false;
    }

    public function buyCart()
    {
        $prodBody = "";
        foreach ($this->product as $key => $product) {
            $prodBody .= '<p>Товар: ' . $this->product[$key] . '</p><div>Колір: <div style="width: 25px; height: 25px; display: inline-block; background-color: '.$this->color[$key].';"></div></div><p>Кількість: ' . $this->count[$key] . '</p><hr>';
        }
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo('cubewood.nature@gmail.com')
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setSubject('Замовлення')
                ->setHtmlBody($prodBody . '
                <p>ПІБ: ' . $this->name . '</p>
                <p>Телефон: ' . $this->phone . '</p>
                <p>Email: ' . $this->email . '</p>
                <p>Населений пункт: ' . $this->city . '</p>
                <p>Адрес: ' . $this->address . '</p>
                <p>Примітка: </p><p>' . $this->notes . '</p>
                <hr>')
                ->send();
            return true;
        }
        return false;
    }
}