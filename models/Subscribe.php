<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%subscribe}}".
 *
 * @property int $id
 * @property string $email
 * @property string $token
 */
class Subscribe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%subscribe}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'token'], 'required'],
            [['email'], 'string', 'max' => 255],
            ['email', 'email'],
            [['token'], 'string', 'max' => 32],
            ['email', 'unique', 'message' => Yii::t('app', 'Данний E-mail уже підписаний на розсилку')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'token' => Yii::t('app', 'Token'),
        ];
    }
}
