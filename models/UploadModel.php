<?php

namespace app\models;

use yii\base\Model;

class UploadModel extends Model
{
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    public function upload($color, $folder, $colorId)
    {
        if ($this->validate()) {
            if (!file_exists('uploads/' . $folder)) {
                mkdir('uploads/' . $folder, 0777);
            }
            if (!file_exists('uploads/' . $folder . '/' . $color)) {
                mkdir('uploads/' . $folder . '/' . $color, 0777);
            }
            $files = glob('uploads/'.$folder.'/'. $color.'/*');
            $name = $colorId.'_'.count($files);
            $extension = $this->imageFile->extension;
            $this->imageFile->saveAs('uploads/' . $folder . '/' . $color . '/' . $name . '.' . $this->imageFile->extension);
            return $name . '.' . $extension;
        } else {
            return false;
        }
    }

}