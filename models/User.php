<?php

namespace app\models;


class User extends \dektrium\user\models\User
{
    public static $usernameRegexp = '/[A-Za-zА-Яа-яЁё\s]/';
}