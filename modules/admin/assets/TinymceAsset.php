<?php
/**
 * Created by PhpStorm.
 * User: ValdemaR
 * Date: 6/6/18
 * Time: 2:38 PM
 */

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

class TinymceAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
        'js/tinymce/tinymce.min.js',
        'js/tinymce-init.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
