<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Coefficients;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CoefficientsController implements the CRUD actions for Coefficients model.
 */
class CoefficientsController extends Controller
{
    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $model = $this->findModel(1);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('/admin');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Coefficients|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Coefficients::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
