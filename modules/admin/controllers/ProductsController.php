<?php

namespace app\modules\admin\controllers;

use app\models\Category;
use app\models\UploadModel;
use Yii;
use app\models\Products;
use app\modules\admin\models\ProductsSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-color' => ['POST'],
                    'delete-all-images' => ['POST'],
                    'image-upload' => ['POST'],
                    'image-delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();
        $model->imageFolder = uniqid();
        $categorys = ArrayHelper::map(Category::find()->all(), 'id', 'name');
        $model->discount = 0;
        if ($model->load(Yii::$app->request->post())) {
            $model->position = Products::find()->max('position')+1;
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'categorys' => $categorys,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->loadModel();
        $categorys = ArrayHelper::map(Category::find()->all(), 'id', 'name');
        if ($model->load(Yii::$app->request->post())) {
            if (file_exists('cache/' . $model->url)) {
                $files = glob('cache/' . $model->url . '/*');
                foreach ($files as $file) {
                    unlink($file);
                }
                rmdir('cache/' . $model->url);
            }
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'categorys' => $categorys,
        ]);
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (file_exists('cache/' . $model->url)) {
            $files = glob('cache/' . $model->url . '/*');
            foreach ($files as $file) {
                unlink($file);
            }
            rmdir('cache/' . $model->url);
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionImageUpload()
    {
        $color = Yii::$app->request->post('color');
        $colorId = Yii::$app->request->post('colorId');
        $folder = Yii::$app->request->post('folder');
        $color = str_replace('#', '', $color);
        $uploadModel = new UploadModel();
        $uploadModel->imageFile = UploadedFile::getInstanceByName('colorImages');
        $name = $uploadModel->upload($color, $folder, $colorId);
        return $this->asJson([
            'error' => $uploadModel->getErrors('imageFile'),
            'initialPreview' => [
                Html::img('/uploads/' . $folder . '/' . $color . '/' . $name, ['class' => 'file-preview-image'])
            ],
            'overwriteInitial' => false,
            'initialPreviewAsData' => true,
            'deleteUrl' => Url::to(['/admin/products/image-delete']),
            'initialPreviewConfig' => [
                0 => [
                    'caption' => $name,
                    'url' => Url::to(['/admin/products/image-delete']),
                    'extra' => ['color' => $color, 'name' => $name, 'folder' => $folder],
                ],
            ],
            'append' => true,
            'initialPreviewThumbTags' => [

            ],
        ]);
    }

    public function actionImageDelete()
    {
        $color = Yii::$app->request->post('color');
        $color = str_replace('#', '', $color);
        $name = Yii::$app->request->post('name');
        $folder = Yii::$app->request->post('folder');
        if (file_exists('uploads/' . $folder . '/' . $color . '/' . $name)) {
            unlink('uploads/' . $folder . '/' . $color . '/' . $name);
        }
        $imgfilse = glob('uploads/' . $folder . '/' . $color . '/*');
        if (empty($imgfilse)) {
            rmdir('uploads/' . $folder . '/' . $color);
        }
        $colorFolders = glob('uploads/' . $folder . '/*');
        if (empty($colorFolders)) {
            rmdir('uploads/' . $folder);
        }
        return $this->asJson('OK');
    }

    public function actionDeleteAllImages()
    {
        $fold = Yii::$app->request->post('folder');
        $can = Yii::$app->request->post('can');
        if ($can == "true") {
            if (file_exists('uploads/' . $fold . '/')) {
                $folders = glob('uploads/' . $fold . '/*');
                foreach ($folders as $folder) {
                    foreach (glob($folder . '/*.*') as $file) {
                        unlink($file);
                    }
                    rmdir($folder);
                }
                rmdir('uploads/' . $fold);
            }
        }
    }

    public function actionLoadImages()
    {
        $color = Yii::$app->request->post('color');
        $folder = Yii::$app->request->post('folder');
        $color = str_replace('#', '', $color);
        if (file_exists('uploads/' . $folder . '/' . $color)) {
            return $this->asJson(['names' => glob('uploads/' . $folder . '/' . $color . '/*')]);
        } else {
            return $this->asJson('empty');
        }
    }

    public function actionDeleteColor()
    {
        $color = Yii::$app->request->post('color');
        $folder = Yii::$app->request->post('folder');
        $color = str_replace('#', '', $color);
        if (file_exists('uploads/' . $folder . '/' . $color)) {
            foreach (glob('uploads/' . $folder . '/' . $color . '/*') as $file) {
                unlink($file);
            }
            rmdir('uploads/' . $folder . '/' . $color);
            $colorFolders = glob('uploads/' . $folder . '/*');
            if (empty($colorFolders)) {
                rmdir('uploads/' . $folder);
            }
        }
    }

    public function actionSort()
    {
        $names = Yii::$app->request->post('names');
        $colorId = Yii::$app->request->post('colorId');
        $folder = Yii::$app->request->post('folder');
        $color = Yii::$app->request->post('color');
        $color = str_replace('#', '', $color);
        $secret = Yii::$app->request->post('secret');
        if ($secret == md5($folder . 'hhsjj22as__2@a')) {
            mkdir('uploads/' . $folder . '/' . $color . '/temp', 0777);
            foreach ($names as $k => $name) {
                $type = explode('.', $name);
                $type = $type[1];
                rename('uploads/' . $folder . '/' . $color . '/' . $name, 'uploads/' . $folder . '/' . $color . '/temp/' . $k . '.' . $type);
            }
            $tempImgs = glob('uploads/' . $folder . '/' . $color . '/temp/*');
            $newNames = [];
            foreach ($tempImgs as $k => $name) {
                $nname = explode('/', $name);
                $nname = $nname[4];
                $type = explode('.', $nname);
                $type = $type[1];
                $newNames[] = $colorId . '_' . $k . '.' . $type;
                rename($name, 'uploads/' . $folder . '/' . $color . '/' . $colorId . '_' . $k . '.' . $type);
            }
            rmdir('uploads/' . $folder . '/' . $color . '/temp');
            return $this->asJson($newNames);
        }
    }
}
