<?php
/**
 * Created by PhpStorm.
 * User: Athlon
 * Date: 28.09.2018
 * Time: 22:52
 */

namespace app\modules\admin\controllers;


use app\models\Products;
use Yii;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;

class SortController extends Controller
{
    public function actionIndex()
    {
        $models = Products::find()->orderBy('position')->all();
        $content = [];
        foreach ($models as $product) {
            $colors = glob('uploads/' . $product->imageFolder . '/*');
            foreach ($colors as $color) {
                $files = glob($color . '/*');
                foreach ($files as $file) {
                    $content[] = ['content' => '<div class="item" id="'.$product->id.'"><img data-position="' . $product->position . '" src="' . '/' . $product->imageCrop($file, 300, 300, $product->url) . '"><div class="name">' . $product->name . '</div></div>'];
                    break;
                }
                if (empty($files)) {
                    $content[] = ['content' => '<div class="item" id="'.$product->id.'"><img data-position="' . $product->position . '" src="/img/no_image.png"><div class="name">' . $product->name . '</div></div>'];
                }
                break;
            }
            if (empty($colors)) {
                $content[] = ['content' => '<div class="item" id="'.$product->id.'"><img data-position="' . $product->position . '" src="/img/no_image.png"><div class="name">' . $product->name . '</div></div>'];
            }
        }
        return $this->render('index', ['content' => $content]);
    }

    public function actionDo()
    {
        $ids = Yii::$app->request->post('ids');
        for ($i = 0; $i < count($ids); $i++){
            $model = Products::findOne($ids[$i]);
            if($model->position != $i) {
                $model->position = $i;
                $model->save();
            }
        }
    }
}