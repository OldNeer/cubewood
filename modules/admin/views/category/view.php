<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'url',
            [
                'attribute' => 'is_label',
                'value' => function ($model) {
                    return $model->is_label ? Yii::t('app', 'Так') : Yii::t('app', 'Ні');
                }
            ],
            [
                'attribute' => 'in_nav',
                'value' => function ($model) {
                    return $model->in_nav ? Yii::t('app', 'Так') : Yii::t('app', 'Ні');
                }
            ],
            [
                'label' => Yii::t('app', 'Батько'),
                'attribute' => 'parent_name',
                'value' => function ($model) {
                    return $model->parent->name;
                },
            ],
        ],
    ]) ?>

</div>
