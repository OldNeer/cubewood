<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Coefficients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coefficients-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dropshipping')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'wholesale')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'wholesale50')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
