<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Coefficients */

$this->title = Yii::t('app', 'Коефіцієнти');
$this->params['breadcrumbs'][] = Yii::t('app', 'Коефіцієнти');
?>
<div class="coefficients-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
