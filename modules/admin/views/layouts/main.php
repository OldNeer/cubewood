<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\admin\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('app', 'Головна'), 'url' => ['/admin'], 'active' => Yii::$app->controller->id == 'products'],
            ['label' => Yii::t('app', 'Категорії'), 'url' => ['/admin/category'], 'active' => Yii::$app->controller->id == 'category'],
            ['label' => Yii::t('app', 'Відгуки'), 'url' => ['/admin/testimonials'], 'active' => Yii::$app->controller->id == 'testimonials'],
            ['label' => Yii::t('app', 'Коефіцієнти'), 'url' => ['/admin/coefficients'], 'active' => Yii::$app->controller->id == 'coefficients'],
            ['label' => Yii::t('app', 'Сортування'), 'url' => ['/admin/sort'], 'active' => Yii::$app->controller->id == 'sort'],
            ['label' => Yii::t('app', 'Посилання'), 'items' =>[
                ['label' => Yii::t('app', 'Cubewood in process'), 'url' => 'https://docs.google.com/spreadsheets/d/1fysQKbANXOQyV7xnlsstGApFtT4Wbr4ROXulZk4AE3M', 'linkOptions' => ['target' => '_blank']],
                ['label' => Yii::t('app', 'Prices'), 'url' => '/prices', 'linkOptions' => ['target' => '_blank']],
            ]],
            Yii::$app->user->isGuest ? (
            ['label' => Yii::t('app', 'Увійти'), 'url' => ['/user/security/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/user/security/logout'], 'post')
                . Html::submitButton(
                    Yii::t('app', 'Вийти') . '(' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => ['label' => Yii::t('app', 'Головна'), 'url' => '/admin'],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?=Alert::widget()?>
        <?= $content ?>
    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
