<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;

\app\modules\admin\assets\TinymceAsset::register($this);
$this->registerJsVar('secret', md5($model->imageFolder . 'hhsjj22as__2@a'));
$this->registerJsVar('folder', $model->imageFolder);
$this->registerJsFile('@web/js/colorselector.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerJsFile('@web/js/characteristic.js', ['depends' => \yii\web\JqueryAsset::className()]);
/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'imageFolder')->textInput(['class' => 'hidden'])->label(false) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'discount')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'enableColor')->widget(SwitchInput::className(), [
        'pluginOptions' => [
            'animate' => false,
            'size' => 'mini'
        ]
    ]) ?>
    <label class="control-label"><?= Yii::t('app', 'Колір') ?></label>
    <div class="add-color">
        <?= ColorInput::widget([
            'name' => 'color',
            'value' => '#000000',
            'options' => [
                'class' => 'color-selector',
            ],
        ]);
        ?>
        <button type="button"><i class="fas fa-plus"></i></button>
    </div>
    <?= $form->field($model, 'colors')->textInput(['maxlength' => true, 'class' => 'hidden'])->label(false); ?>
    <div class="selected-colors">
        <?php $colors = explode(';', $model->colors);
        array_pop($colors) ?>
        <?php foreach ($colors as $color): ?>
            <div class='color' data-color='<?= $color ?>' style='background: <?= $color ?>'></div>
        <?php endforeach; ?>
    </div>
    <?=
    FileInput::widget([
        'name' => 'colorImages',
        'options' => ['accept' => 'image/*'],
        'options' => [
            'multiple' => true,
            'class' => 'imageSelect',
        ],
        'pluginOptions' => [
            'uploadUrl' => Url::to(['/admin/products/image-upload']),
            'uploadExtraData' => new JsExpression('function(){return {color: $(".color.can-close").data("color"), folder: "' . $model->imageFolder . '"}}'),
            'overwriteInitial' => false,
            'showRemove' => true,
            'theme' => "explorer",
        ],
    ]);
    ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 10, 'class' => 'tinymce']) ?>

    <?= $form->field($model, 'detalization')->textInput(['maxlength' => true]) ?>
    <strong class="control-label"><?= Yii::t('app', 'Характеристика') ?></strong>
    <table class="characteristic table">
        <thead>
        <tr>
            <th><?= Yii::t('app', 'Назва') ?></th>
            <th><?= Yii::t('app', 'Текст') ?></th>
            <th width="38"></th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($model->characteristicName) == 0): ?>
            <tr class="main">
                <td>
                    <?= $form->field($model, 'characteristicName[]')->textInput(['maxlength' => true])->label(false); ?>
                </td>
                <td>
                    <?= $form->field($model, 'characteristicText[]')->textInput(['maxlength' => true])->label(false); ?>
                </td>
                <td>
                    <?= Html::button('<i class="fas fa-plus"></i>', ['class' => 'btn btn-primary characteristic-button add']) ?>
                </td>
            </tr>
        <?php else: ?>
            <?php $first = true ?>
            <?php foreach ($model->characteristicName as $k => $name): ?>
                <?php if ($first): ?>
                    <?php $first = false; ?>
                    <tr class="main">
                        <td>
                            <?= $form->field($model, 'characteristicName[]')->textInput(['maxlength' => true, 'value' => $name])->label(false); ?>
                        </td>
                        <td>
                            <?= $form->field($model, 'characteristicText[]')->textInput(['maxlength' => true, 'value' => $model->characteristicText[$k]])->label(false); ?>
                        </td>
                        <td>
                            <?= Html::button('<i class="fas fa-plus"></i>', ['class' => 'btn btn-primary characteristic-button add']) ?>
                        </td>
                    </tr>
                <?php else: ?>
                    <tr>
                        <td>
                            <?= $form->field($model, 'characteristicName[]')->textInput(['maxlength' => true, 'value' => $name])->label(false); ?>
                        </td>
                        <td>
                            <?= $form->field($model, 'characteristicText[]')->textInput(['maxlength' => true, 'value' => $model->characteristicText[$k]])->label(false); ?>
                        </td>
                        <td>
                            <?= Html::button('<i class="fas fa-times"></i>', ['class' => 'btn btn-danger characteristic-button remove']) ?>
                        </td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
    <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
        'data' => $categorys,
    ]); ?>

    <?= $form->field($model, 'priceDesc')->textarea(['rows' => 5]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
