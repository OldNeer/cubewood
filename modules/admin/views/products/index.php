<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Products'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'url:text',
            'price',
            'discount',
            [
                'attribute' => 'category_id',
                'label' => Yii::t('app', 'Категорія'),
                'value' => 'category.name'
            ],
            [
                'attribute' => 'sel-colors',
                'label' => Yii::t('app', 'Кольори'),
                'format' => 'raw',
                'value' => function ($model) {
                    $return = '<div class="selected-colors" style="margin-bottom: 0px">';
                    $colors = explode(';', $model->colors);
                    array_pop($colors);
                    foreach ($colors as $color) {
                        $return .= '<div class="color" data-color="' . $color . '" style="background: ' . $color . '; margin-bottom: 5px"></div>';
                    }
                    $return .= '</div>';
                    return $return;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
