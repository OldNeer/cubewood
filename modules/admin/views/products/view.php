<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('yii','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'url',
            'detalization',
            'price',
            'discount',
            [
                'attribute' => 'enableColor',
                'value' => function ($model) {
                    return $model->enableColor ? Yii::t('app', 'Так') : Yii::t('app', 'Ні');
                }
            ],
            [
                'attribute' => 'sel-colors',
                'label' => Yii::t('app', 'Кольори'),
                'format' => 'raw',
                'value' => function ($model) {
                    $return = '<div class="selected-colors" style="margin-bottom: 0px">';
                    $colors = explode(';', $model->colors);
                    array_pop($colors);
                    foreach ($colors as $color) {
                        $return .= '<div class="color" data-color="' . $color . '" style="background: ' . $color . '; margin-bottom: 5px"></div>';
                    }
                    $return .= '</div>';
                    return $return;
                }
            ],
            'description:html',
            [
                'label' => Yii::t('app', 'Категорія'),
                'attribute' => 'category.name',
            ],
            'priceDesc',
        ],
    ]) ?>

</div>
