<?php

use kartik\sortable\Sortable;
use yii\helpers\Html;

/* @var $this \yii\web\View */

$this->registerJsFile('/js/sort.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>
<div class="wrap content">
    <?= Sortable::widget([
        'type' => 'grid',
        'items' => $content,
    ]); ?>
    <?= Html::button(Yii::t('app', 'Зберегти'), ['class' => 'save btn btn-success']) ?>
</div>
