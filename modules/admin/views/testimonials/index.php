<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\tabs\TabsX;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\TestimonialsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCss('.summary{
display:none;
}
.tab-content{
padding: 0;
}
.nav-tabs{
border-bottom: none;
}');

$this->title = Yii::t('app', 'Testimonials');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testimonials-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Testimonials'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin() ?>
    <?= TabsX::widget([
        'items' => [
            [
                'label' => 'Не активні',
                'content' => GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'text:ntext',
                        'username',
                        'date',

                        ['class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {delete} {update} {active}',
                            'buttons' => [
                                'active' => function ($url, $model) {
                                    return Html::a('<i class="fas fa-check"></i>', '/admin/testimonials/active?id=' . $model->id, ['title' => 'Деактивувати']);
                                }
                            ],],
                    ],
                ])
            ],
            [
                'label' => 'Активні',
                'content' => GridView::widget([
                    'dataProvider' => $activeDataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'text:ntext',
                        'username',
                        'date',

                        ['class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {delete} {update} {deactive}',
                            'buttons' => [
                                'deactive' => function ($url, $model) {
                                    return Html::a('<i class="fas fa-times"></i>', '/admin/testimonials/deactive?id=' . $model->id, ['title' => 'Деактивувати']);
                                }
                            ],
                        ],
                    ],
                ])
            ]
        ],
        'position' => TabsX::POS_ABOVE,
        'encodeLabels' => false
    ]); ?>
    <?php Pjax::end() ?>
</div>
