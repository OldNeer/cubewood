<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use app\assets\AppAsset;

header('Cache-Control: public, max-age=3600');
AppAsset::register($this);
$categorys = empty($this->params['categorys']) ? [] : $this->params['categorys'];
$cart = empty($this->params['cart']) ? [] : $this->params['cart'];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords"
          content="concrete,concretestyle,moderndesign,loft,loftstyle,loft,pot,concretepot,flowers,квіти,вазоны,бетон,современныйинтерьер,современныйдизайн,лофт,лофтстиль,бетоннийгорщик,горшокдляцветов,интерьер,бетонвинтерьере,цветы,vase,concretevase,loveflowers,lviv,kyiv,lvivinteriordesign,lvivgram,kyivgram"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="eclipse"></div>
<div class="mobile-nav">
    <div class="close">
        <i class="fas fa-times"></i>
    </div>
    <div class="wrap">
        <h2 class="nav navbar-title"><?= Yii::t('app', 'Меню') ?></h2>
        <?php if (!empty($categorys)): ?>
            <?php foreach ($categorys as $category): ?>
                <?php if ($category->parent_id == null): ?>
                    <ul class="nav flex-column pjax">
                        <?php if ($category->is_label): ?>
                            <li class="head"><?= $category->name ?></li>
                        <?php else: ?>
                            <li><a href="/<?= $category->url ?>"><?= $category->name ?></a></li>
                        <?php endif; ?>
                        <?php foreach ($categorys as $category_): ?>
                            <?php if ($category_->parent_id == $category->id): ?>
                                <?php if ($category_->is_label): ?>
                                    <li class="head"><?= $category_->name ?></li>
                                <?php else: ?>
                                    <li><a href="/<?= $category_->url ?>"><?= $category_->name ?></a></li>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                    <hr>
                <?php endif ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <ul class="nav flex-column">
            <li class="head"><?= Yii::t('app', 'Інформація') ?></li>
            <li><a href="/aboutus"><?= Yii::t('app', 'Про нас') ?></a></li>
            <li><a href="/payment-and-delivery"><?= Yii::t('app', 'Оплата і доставка') ?></a></li>
            <li><a href="/testimonials"><?= Yii::t('app', 'Відгуки') ?></a></li>
            <li><a href="/#"><?= Yii::t('app', 'Співпраця') ?></a></li>
        </ul>
        <hr>
        <ul class="nav flex-column">
            <li><a href="/#"><?= Yii::t('app', 'Блог') ?></a></li>
        </ul>
        <hr>
        <ul class="nav flex-column">
            <li class="head">Контакти</li>
            <li><a>+38 097 6248321</a></li>
            <li><a>+38 093 8160499</a></li>
            <li><a>cubewood.nature@gmail.com</a></li>
        </ul>
    </div>
</div>
<div class="wrap">
    <div class="container">
        <header>
            <?php
            NavBar::begin([
                'brandImage' => '/img/logo.png',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar navbar-fixed-top',
                ],
            ]);
            $categoryItems = [];
            foreach ($categorys as $category) {
                if ($category->parent_id == null) {
                    if($category->in_nav) {
                        $childItems = [];
                        foreach ($categorys as $category_) {
                            if ($category_->parent_id == $category->id) {
                                $childItems[] = ['label' => $category_->name, 'url' => '/' . $category_->url, 'options' => ['class' => 'nav-manu']];
                            }
                        }
                        if (!empty($childItems)) {
                            array_push($categoryItems, ['label' => $category->name, 'items' => $childItems, 'options' => ['class' => 'nav-menu']]);
                        } else {
                            array_push($categoryItems, ['label' => $category->name, 'url' => $category->url, 'options' => ['class' => 'nav-menu']]);
                        }
                    }
                }
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-left'],
                'encodeLabels' => false,
                'items' =>
                    array_merge($categoryItems, [
                        ['label' => '+38 097 6248321',
                            'items' => [
                                ['label' => '+38 093 8160499'],
                                ['label' => 'cubewood.nature@gmail.com'],
                            ],
                            'options' => ['class' => 'contact-nav']
                        ],
                        ['label' => Yii::t('app', 'Про нас'), 'url' => ['/aboutus'], 'options' => ['class' => 'about-as']],
                        ['label' => Yii::t('app', 'Оплата і доставка'), 'url' => ['/payment-and-delivery'], 'options' => ['class' => 'payndelivery']],
                        ['label' => '<i class="fab fa-instagram"></i>', 'url' => Url::to('https://www.instagram.com/cube_wood'), 'options' => ['class' => 'social inst']],
                        ['label' => '<i class="fab fa-facebook-f"></i>', 'url' => Url::to('https://www.facebook.com/cubwood'), 'options' => ['class' => 'social face']],
                        ['label' => '<i class="fab fa-pinterest-p"></i>', 'url' => ['/'], 'options' => ['class' => 'disabled social pin']],//https://www.pinterest.com/cubewood0109/pins/
                        ['label' => '<i class="fab fa-etsy"></i>', 'url' => ['/'], 'options' => ['class' => 'disabled social etsy']],//https://www.etsy.com/shop/Cubewood
                    ]),
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'encodeLabels' => false,
                'items' => [
                    ['label' => '<i class="fas fa-shopping-cart"></i>', 'url' => ['/cart'], 'options' => ['class' => 'cart-button', 'title' => Yii::t('app', 'Корзина')]],
                    ['label' => '<i class="fas fa-search"></i>', 'url' => ['#'], 'options' => ['class' => 'search-button hidable2', 'title' => 'Пошук']],
                    ['label' => '<i class="fas fa-bars"></i>', 'options' => ['class' => 'open-menu', 'title' => Yii::t('app', 'Меню')]],
                ],
            ]);
            NavBar::end();
            ?>
            <?= $form = Html::beginForm('/', 'GET', ['class' => 'search-form']) ?>
            <?= Html::submitButton('<i class="fas fa-search"></i>') ?>
            <?= Html::input('string', 'search', Yii::$app->request->get('search'), ['placeholder' => Yii::t('app', 'Пошук')]) ?>
            <?= Html::button('<i class="fas fa-times"></i>', ['class' => 'search-close']) ?>
            <?= Html::endForm() ?>
        </header>
        <?= $content ?>
    </div>
</div>
<footer>
    <div class="container">
        <div class="wrap">
            <div class="links">
                <ul>
                    <li><a href="/payment-and-delivery"><?= Yii::t('app', 'Оплата і доставка') ?></a></li>
                    <li><a href="#"><?= Yii::t('app', 'Співпраця') ?></a></li>
                    <li><a href="/testimonials"><?= Yii::t('app', 'Відгуки') ?></a></li>
                    <li><a href="#"><?= Yii::t('app', 'Блог') ?></a></li>
                </ul>
            </div>
            <div class="footer-group footer-group-subscribe">
                <div class="footer-subtitle"><?= Yii::t('app', 'Підписатись на новини та спеціальні пропозиції') ?></div>
                <form class="footer-subscribe">
                    <input class="footer-input" required placeholder="<?= Yii::t('app', 'Пошта') ?>" type="email">
                    <button class="footer-submit" type="submit"><i class="fas fa-arrow-right"></i></button>
                </form>
            </div>
            <div class="footer-social">
                <span><?= Yii::t('app', 'Ми в соцмережах') ?></span>
                <ul>
                    <li class="social"><a href="https://www.instagram.com/cube_wood"><i
                                    class="fab fa-instagram"></i></a></li>
                    <li class="social"><a href="https://www.facebook.com/cubwood"><i
                                    class="fab fa-facebook-square"></i></i></a></li>
                    <li class="social disabled"><a href=""><i class="fab fa-pinterest"></i></i></a></li>
                    <!--https://www.pinterest.com/cubewood0109/pins/-->
                </ul>
            </div>
            <div class="footer-contact">
                <p><?= Yii::t('app', 'Телефон') ?></p>
                <p class="cont-phone">+38 097 6248321</p>
                <p>E-mail</p>
                <p class="cont-email">cubewood.nature@gmail.com</p>
            </div>
        </div>
        <div class="rights">
            Cubewood © Всі права захищені
        </div>
    </div>
    <?php Modal::begin(['options' => ['class' => 'subscribe-modal'], 'bodyOptions' => ['class' => 'subscribe-modal-body']]) ?>

    <?php Modal::end() ?>
</footer>
<?= $this->render('@app/views/site/_cart', ['cart' => $cart]); ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
