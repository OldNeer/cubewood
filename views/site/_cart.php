<?php $this->registerCssFile('@web/css/cart.css'); ?>
<div class="cart-content">
    <?= generate($cart, $this) ?>
</div>
<?php
/* @var $this \yii\web\View */
$this->registerJs('
    var timeout;
    $(".cart-button").popover({
        selector: ".cart-content",
        content: function () {
        return $(".cart-content").html();
        },
        container: "header",
        placement: "bottom",
        trigger: "manual",
        html: true,
    }).on("mouseenter", function () {
        clearTimeout(timeout);
        var _this = this;
        if(!$(".popover").length){
        $(this).popover("show");
        $(this).siblings(".popover").on("mouseleave", function () {
            $(_this).popover(\'hide\');
        });
        }
    });
    $(document).on("mouseleave",".popover, .cart-button", function (e) {
        var _this = $(".cart-button");
        timeout = setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide")
            }
        }, 200);
    });
    $(window).resize(function(){$(".cart-button").popover("hide")});
    ');
function generate($cart, $_this)
{
    if (empty($cart)) {
        $_this->registerJsVar('totalPrice', 0);
        return '<div class="well well-lg">' . Yii::t('app', 'Ваша корзина пуста!') . '</div>';
    }
    $content = "";
    $total = 0;
    foreach ($cart as $c) {
        $content .= "<div id='".$c->id."' class='row cart_line'><div class='col-xs-3'><span><img class='img img-responsive img-rounded' src='$c->images'></span></div><div class='col-xs-9'><a href='/product/$c->url'><span class='h6'>$c->name</span></a><span class='d-block text-muted'>".Yii::t('app', 'Кількість').": <span class='count'>$c->count</span></span><span class='text-muted'>$c->price " . Yii::t('app', 'UAH') . "</span></div></div>";
        $total += $c->price*$c->count;
    }
    $_this->registerJsVar('totalPrice', $total);
    return "<div class='items'>$content</div><div class='text-center'><span class='h5'><div id='cart_total'><div class='row' id='order_total'><span class='col-xs-6 text-right h4'>Всього:</span><span class='col-xs-6 text-left h4' style='white-space: nowrap;'>$total " . Yii::t('app', 'UAH') . "</span></div></div></span><a class='btn btn-primary' href='/cart'>" . Yii::t('app', 'Переглянути кознину') . " (<span class='totalCount'>" . count($cart) . "</span> " . Yii::t('app', 'шт') . ")</a></div>";
}

?>