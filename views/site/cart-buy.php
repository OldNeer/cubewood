<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */

$this->registerCssFile('/css/cart.css');
$this->title = Yii::t('app', 'Інформація для доставки');
?>
<div class="container">
    <ul class="wizard pull-right">
        <li id="step10" class="text-muted">

            <a href="/cart" class="text-success">
                <?= Yii::t('app', 'Переглянути замовлення') ?><span class="chevron"></span>
            </a>


        </li>
        <li id="step20" class="text-primary">
            <?= Yii::t('app', 'Доставка і Оплата') ?><span class="chevron"></span>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-9 oe_cart">
            <div class="row">

                <h3 class="page-header"><?= Yii::t('app', 'Інформація для доставки'); ?></h3>
                <?php $form = ActiveForm::begin() ?>
                <?= $form->field($purchaseForm, 'name')->textInput(['placeholder' => 'наприклад Боднаренко Назар Борисович']) ?>
                <?= $form->field($purchaseForm, 'phone') ?>
                <?= $form->field($purchaseForm, 'email') ?>
                <?= $form->field($purchaseForm, 'city') ?>
                <?= $form->field($purchaseForm, 'address') ?>
                <?= $form->field($purchaseForm, 'notes')->textarea(['placeholder' => Yii::t('app', 'Не обов\'язково')]) ?>
                <?php $total = 0; ?>
                <?php foreach ($items as $item): ?>
                    <?php $total += $item->price * $item->count ?>
                    <?= $form->field($purchaseForm, 'product[]')->textInput(['value' => $item->name, 'class' => 'hidden'])->label(false) ?>
                    <?= $form->field($purchaseForm, 'color[]')->textInput(['value' => $item->selColor, 'class' => 'hidden'])->label(false) ?>
                    <?= $form->field($purchaseForm, 'count[]')->textInput(['value' => $item->count, 'class' => 'hidden'])->label(false) ?>
                <?php endforeach; ?>
                <div class="clearfix"></div>
                <div>
                    <a class="btn btn-default mb32" href="/cart">
                        <i class="fas fa-arrow-left"></i> <?= Yii::t('app', 'Назад') ?>
                    </a>
                    <?= Html::submitButton("<span>" . Yii::t('app', 'Підтвердити') . "</span> <i class='fas fa-check'></i>", ['class' => 'btn btn-default btn-primary pull-right mb32 a-submit a-submit-disable a-submit-loading']) ?>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 text-muted info">
            <h3 class="page-header mt16 text-center"><?= Yii::t('app', 'Ваше замовлення') ?></h3>


            <div id="cart_total" class="">
                <div class="row" id="order_total">
                    <span class="col-xs-6 text-right h4"><?= Yii::t('app', 'Всього') ?>:</span>
                    <span class="col-xs-6 text-left h4" style="white-space: nowrap;">
                    <span style="white-space: nowrap;"><span
                                class="oe_currency_value"><?= sprintf("%.2f", $total) ?></span>&nbsp;грн</span>
                </span>
                </div>
            </div>

        </div>
    </div>
</div>