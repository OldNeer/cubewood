<?php
/* @var $this \yii\web\View */

$this->title = Yii::t('app', 'Корзина');

$this->registerJsVar('empty', Yii::t('app', 'Ваша корзина пуста!'));
$this->registerCssFile('/css/cart.css');
$this->registerJsFile('/js/cart.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>
<div class="container">
    <ul class="wizard pull-right">
        <li class="text-primary">
            <?= Yii::t('app', 'Переглянути замовлення') ?><span class="chevron"></span>
        </li>
        <li class="text-muted">
            <?= Yii::t('app', 'Інформація для доставки') ?><span class="chevron"></span>
        </li>
    </ul>
    <h1><?= Yii::t('app', 'Корзина') ?></h1>
    <div class="row">
        <div class="col-md-8 col-sm-9 oe_cart">
            <?php if (!empty($items)): ?>
                <table class="table table-striped table-condensed" id="cart_products">
                    <thead>
                    <tr>
                        <th class="d-image" width="100"></th>
                        <th><?= Yii::t('app', 'Назва') ?></th>
                        <th width="130" class="text-center"><?= Yii::t('app', 'Кількість') ?></th>
                        <th width="100" class="text-center"><?= Yii::t('app', 'Ціна') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $total = 0 ?>
                    <?php foreach ($items

                                   as $item): ?>
                        <?php $total += $item->price * $item->count ?>
                        <tr>
                            <td class="d-image" align="center">
                                <span><img class="img img-responsive img-rounded" src="<?= $item->images ?>"></span>
                            </td>
                            <td>
                                <div>
                                    <a href="/product/<?= $item->url ?>">
                                        <strong><?= $item->name ?></strong>
                                    </a>
                                </div>
                                <div class="text-muted">
                                </div>
                                <a href="#" id="<?= $item->id ?>" class="cart-delete">
                                    <small><i class="far fa-trash-alt"></i> Видалити</small>
                                </a>
                                <?php if ($item->category->url != 'decor' && $item->enableColor): ?>
                                    <div class="sel-color-cart>
                                        <?php $colors = explode(';', $item->colors);
                                        array_pop($colors);
                                        ?>
                                        <?php foreach ($colors as $color): ?>
                                            <?php if ($color != $item->selColor): ?>
                                                <div data-id="<?= $item->id ?>" data-color="<?= $color ?>" class='color'
                                                     style='background: <?= $color ?>'></div>
                                            <?php else: ?>
                                                <div data-color="<?= $color ?>" data-id="<?= $item->id ?>"
                                                     class='color selected'
                                                     style='background: <?= $color ?>'></div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </td>
                            <td class="text-center" id="td-qty">
                                <div class="input-group oe_website_spinner">
                                    <a class="input-group-addon count-minus" data-no-instant="" href="#">
                                        <i class="fa fa-minus"></i>
                                    </a>
                                    <input type="text" class="form-control quantity count" id="<?= $item->id ?>"
                                           value="<?= $item->count ?>">
                                    <a class="input-group-addon float_left count-add" data-no-instant=""
                                       href="#">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <span class="text-danger count-error"><?= Yii::t('app', 'Не більше 10 і не менше 1') ?></span>
                                </div>

                            </td>
                            <td class="text-center" id="td-price" name="price">
                        <span style="white-space: nowrap;"><span
                                    class="price"><?= sprintf("%.2f", $item->price) ?></span> <?= Yii::t('app', 'UAH') ?></span>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                <div class="js_cart_lines">

                    <div id="cart_total" class="col-sm-4 col-sm-offset-8 col-xs-12">
                        <div class="row">
                            <span class="col-xs-6 text-right h4">Всього:</span>
                            <span class="col-xs-6 text-left h4" style="white-space: nowrap;">
                    <span style="white-space: nowrap;"><span
                                class="totalprice"><?= sprintf("%.2f", $total) ?></span> <?= Yii::t('app', 'UAH') ?></span>
                </span>
                        </div>
                    </div>

                </div>

                <div class="clearfix"></div>
                <a href="/cart/buy" class="btn btn-default pull-right">
                    <?= Yii::t('app', 'Замовити') ?> <i class="fas fa-arrow-right"></i>
                </a>
            <?php else: ?>
                <div class="well well-lg">
                    <?= Yii::t('app', 'Ваша корзина пуста!') ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>