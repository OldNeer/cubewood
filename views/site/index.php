<?php

use yii\bootstrap\Alert;
use \yii\helpers\Html;
use evgeniyrru\yii2slick\Slick;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
$this->registerJsVar('index', true);
$this->registerJsFile('@web/js/textcutter.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerJs('    
var timer;
var timeout;
    $(document).on("mouseenter", ".content .item", function () {
        var item = $(this)
        timer = setInterval(function () {
            item.find(".item-glr").slick("slickNext");
        },1200)
        $(this).mouseleave(function () {
            clearInterval(timer);
            var item = $(this)
            timeout = setTimeout(function(){
            item.find(".item-glr").slick("slickGoTo",0);
            },2000)
        })
        $(this).mouseenter(function(){
            clearTimeout(timeout);
        })
    });
    $(document).ready(function(){
    $(".item-glr > div").not(".slick-list").not(":first-child").remove()
    $(window).resize(function(){if($(window).width() <= 670){
    $(".item-glr > div").not(".slick-list").not(":first-child").remove()}});
    $(".load").css("display", "none");
    $(".wrap.content").css("display", "grid");
    $(".response-wrap").css("display", "block")})
    ');
$this->title = Yii::t('app', 'Сubewood – сучасний дизайн створений з дерева');
if (Yii::$app->session->hasFlash('message')) {
    Alert::begin([
        'options' => [
            'class' => 'alert-info',
        ],
    ]);
    echo Yii::$app->session->get('message');
    Alert::end();
}
?>
<div class="load">
    <div class="lds-facebook">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<main>
    <?php Pjax::begin() ?>
    <?php if ($notfound): ?>
        <div class="text-center">
            <h3><?= Yii::t('app', 'Нічого не знайдено') ?></h3>
        </div>
    <?php endif; ?>
    <div class="wrap content">
        <?php if ($products != null): ?>
            <?php foreach ($products as $k => $product): ?>
                <?php
                if ($product->price[strlen($product->price) - 1] == 0 && $product->price[strlen($product->price) - 2] == 0) {
                    $product->price = str_replace('.00', '', $product->price);
                } ?>
                <div class="item">
                    <a href="/product/<?= $product->url ?>" data-pjax="0">
                        <?php if ($product->discount > 0): ?>
                            <span class="price discount"><?= $product->price ?> UAH</span>
                            <span class="price new"><?= $product->price - ($product->price * ($product->discount / 100)) ?>
                                UAH</span>
                        <?php else: ?>
                            <span class="price"><?= $product->price ?> UAH</span>
                        <?php endif; ?>
                        <?php if (count($product->images) > 1): ?>
                            <?= Slick::widget([
                                'itemContainer' => 'div',
                                'containerOptions' => ['class' => 'item-glr', 'id' => 's' . $k],
                                'items' => $product->images,
                                'clientOptions' => [
                                    'slidesToShow' => 1,
                                    'slidesToScroll' => 1,
                                    'arrows' => false,
                                    'dots' => false,
                                    'responsive' => [
                                        [
                                            'breakpoint' => 670,
                                            'settings' => 'unslick',
                                        ],

                                    ],
                                ],

                            ]); ?>
                        <? else: ?>
                            <?= $product->images[0] ?>
                        <?php endif ?>
                        <div class="name">
                            <?= $product->name ?>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <?php Pjax::end() ?>
</main>
<?= Slick::widget([

    // HTML tag for container. Div is default.
    'itemContainer' => 'div',

    // HTML attributes for widget container
    'containerOptions' => ['class' => 'slider hidden'],


    'items' => [
        Html::img('@web/img/3.jpg'),
        Html::img('@web/img/1.jpg'),
        Html::img('@web/img/3.jpg'),
    ],
    'clientOptions' => [
        'dots' => false,
        'centerMode' => true,
        'centerPadding' => '20%',
        'slidesToShow' => 1,
        'adaptiveHeight' => true,
        'draggable' => false,
        'autoplay' => true,
        'autoplaySpeed' => 2000,
        'nextArrow' => '<a id="next"><img src="/web/img/next.png"></a>',
        'prevArrow' => '<a id="back"><img src="/web/img/prev.png"></a>',
    ],

]); ?>
<div class="response-wrap">
    <div class="title">
        <h1>ВІДГУКИ НАШИХ КЛІЄНТІВ</h1>
        <a href="/testimonials"><?= Yii::t('app', 'Всі відгуки') ?> <i class="fas fa-arrow-right"></i></a>
    </div>
    <div class="response">
        <?php foreach ($testimonials as $k => $testimonial): ?>
            <div class="item">
                <a class="product" href="/product/<?= $testimonial->product->url ?>">
                    <?= Slick::widget([
                        'itemContainer' => 'div',
                        'containerOptions' => ['class' => 'item-glr', 'id' => 't' . $k],
                        'items' => $testimonial->product->images,
                        'clientOptions' => [
                            'slidesToShow' => 1,
                            'slidesToScroll' => 1,
                            'arrows' => false,
                            'autoplay' => true,
                            'autoplaySpeed' => 3000,
                            'speed' => 500,
                            'dots' => false,
                        ],

                    ]); ?>
                </a>
                <div class="text">
                    <a href="/testimonials#<?= $testimonial->product->id ?>"
                       title="<?= Yii::t('app', 'Читати повністю') ?>">
          <span>
            <?= $testimonial->text ?>
          </span>
                        <span class="full-text hidden"><?= $testimonial->text ?></span>
                        <span class="date">(<?= strftime('%d %b. %Y ' . Yii::t('app', 'р.'), strtotime($testimonial->date)) ?>
                            )</span>
                        <a href="<?= $testimonial->userurl ?>"
                           class="from"><?= $testimonial->username ?></a>
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>