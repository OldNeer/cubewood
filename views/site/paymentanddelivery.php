<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<main>
    <div class="main-container">
        <h3>Способи доставки:</h3>
        <br>
        <p> Вироби відправляється Новою поштою або іншим оператором поштового зв’язку України протягом 1-3 робочих
            днів з дня замовлення (якщо обрана клієнтом конфігурація лампи існує в наявності),
            або ж протягом 5-14 робочих днів (якщо настільну лампу потрібно виготовити або дозамовити необхідні деталі).
            <br><br>
            Якщо Ви знаходитесь у Львові, можливо обговорити доставку індивідуально.
            <br><br>
            Доставка за межі України здійснюється Укрпоштою згідно з тарифами і умовами цього оператора поштового
            зв'язку.
        </p>
        <br>
        <br>
        <h3>Способи оплати:</h3>
        <br>
        <p> Оплаа здійснюється в гривнях шляхом переказу коштів на карту Приватбанку або ж оплативши у відділенні
            Нової пошти (іншого оператора) при отриманні замовлення.
            <br><br>
            Оплата товарів для відправлення за межі України здійснюється згідно з діючим курсом валют. При відправленні
            закордон необхідною є 100% передоплата вартості товару і вартості доставки.
        </p>

        <p><strong><i> Завжди раді відповісти на всі Ваші запитання!</i></strong></p>
    </div>
    <br>
    <div class="connection">
        <?php $form = ActiveForm::begin() ?>
        <div class="d-flex">
            <div class="info">
                <?= $form->field($paymentanddelivery, 'name')->textInput(['placeholder' => Yii::t('app', "Ваше ім'я")])->label(false) ?>
                <?= $form->field($paymentanddelivery, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+999999999999',
                    'options' => ['placeholder' => Yii::t('app', 'Телефон')],
                ])->label(false) ?>

                <?= $form->field($paymentanddelivery, 'email')->textInput(['placeholder' => Yii::t('app', "Email")])->label(false) ?>
            </div>
            <?= $form->field($paymentanddelivery, 'text')->textarea(['cols' => '40', 'rows' => '5', 'placeholder' => Yii::t('app', 'Повідомлення')])->label(false) ?>
        </div>
        <?= Html::submitButton(Yii::t('app', 'Відправити')) ?>
        <?php ActiveForm::end() ?>
    </div>

</main>