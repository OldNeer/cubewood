<?php

use evgeniyrru\yii2slick\Slick;

/* @var $this \yii\web\View */

$this->title = Yii::t('app', 'Ціни');


$this->registerJs('
    $(document).on("mouseenter", "table a", function () {
        var item = $(this)
        timer = setInterval(function () {
            item.find(".prices-glr").slick("slickNext");
        },1200)
        $(this).mouseleave(function () {
            clearInterval(timer);
        })
    });
    ')
?>
<table class="table table-condensed prices-table">
    <thead>
    <tr>
        <th></th>
        <th><?= Yii::t('app', 'Роздрібна ціна') ?></th>
        <th><div style="width: 100px"><?= Yii::t('app', 'Ціна (дропшипінг)') ?></div></th>
        <th><div style="width: 100px"><?= Yii::t('app', 'Оптова ціна (від 5 шт)') ?></div></th>
        <th><div style="width: 100px"><?= Yii::t('app', 'Оптова ціна (від 50 шт)') ?></div></th>
        <th><?= Yii::t('app', 'Опис') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($products as $product): ?>
        <tr>
            <td width="250">
                <a href="/product/<?=$product->url?>">
                    <?php if(count($product->images) > 1): ?>
                    <?= Slick::widget([
                        'itemContainer' => 'div',
                        'containerOptions' => ['class' => 'prices-glr'],
                        'items' => $product->images,
                        'clientOptions' => [
                            'slidesToShow' => 1,
                            'slidesToScroll' => 1,
                            'arrows' => false,
                            'dots' => false,
                        ],

                    ]); ?>
                    <?php else:?>
                    <?=$product->images[0]?>
                    <?php endif;?>
                    <div class="name"><?=$product->name?></div>
                </a>
            </td>
            <td><?= $product->price ?></td>
            <td><?= sprintf("%.2f", $product->price * $coefficients->dropshipping) ?></td>
            <td><?= sprintf("%.2f", $product->price * $coefficients->wholesale) ?></td>
            <td><?= sprintf("%.2f", $product->price * $coefficients->wholesale50) ?></td>
            <td width="550"><?=$product->priceDesc?></td>
        </tr>
    <? endforeach; ?>
    </tbody>
</table>
