<?php

use evgeniyrru\yii2slick\Slick;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use \yii\helpers\Html;

/* @var $this yii\web\View */
$this->registerCssFile('@web/css/product.css');
$this->registerJsVar('id', $model->id);
$this->registerJsVar('name', $model->name);
$this->registerJsVar('price', $model->price);
$this->registerJsVar('url', $model->url);
$this->registerJsVar('valuta', Yii::t('app', 'UAH'));
$this->registerJsVar('image', $images[0]);
$this->registerJsVar('empyCart', Yii::t('app', 'Ваша корзина пуста!'));
$cookies = Yii::$app->request->cookies;
$this->registerJsVar('addtitle', Yii::t('app', 'добавити в корзину'));
$this->registerJsVar('deltitle', Yii::t('app', 'видалити з корзини'));
$this->registerJsVar('vsogo', Yii::t('app', 'Всього'));
$this->registerJsVar('st', Yii::t('app', 'шт.'));
$this->registerJsVar('countText', Yii::t('app', 'Кількість'));
$this->registerJsVar('pereglanuty', Yii::t('app', 'Переглянути кознину'));
if ($cookies->has('cart')) {
    $cookie = $cookies->get('cart')->value;
    if (array_key_exists($model->id, $cookie)) {
        $this->registerJsVar('del', true);
    } else {
        $this->registerJsVar('del', false);
    }
} else {
    $this->registerJsVar('del', false);
}
$this->registerJsFile('@web/js/product.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->title = $model->name . ' - CubeWood';
?>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125716904-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', 'UA-125716904-1');
</script>
<div class="main">
    <div class="galery">
        <?php \yii\widgets\Pjax::begin() ?>
        <?= Slick::widget([

            'itemContainer' => 'div',
            'containerOptions' => ['class' => 'glr-main'],

            'items' => $images,
            'clientOptions' => [
                'slidesToShow' => 1,
                'slidesToScroll' => 1,
                'draggable' => true,
                'arrows' => false,
                'dots' => true,
                'autoplay' => true,
                'autoplaySpeed' => 3000,
                'speed' => 300,
            ],

        ]); ?>
        <?php \yii\widgets\Pjax::end() ?>
    </div>
    <div class="information">
        <h1 class="name"><?= $model->name ?></h1>
        <span class="detalization"><?= $model->detalization ?></span>
        <?php if ($model->category->url != 'decor' && $model->enableColor): ?>
            <div class="sel-color">
                <?php $colors = explode(';', $model->colors);
                array_pop($colors);
                $this->registerJsVar('defColor', $colors[0]);
                ?>
                <?php foreach ($colors as $color): ?>
                    <?php if (file_exists('uploads/' . $model->imageFolder . '/' . str_replace('#', '', $color))): ?>
                        <div data-color="<?= $color ?>" class='color' style='background: <?= $color ?>'></div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?php if ($model->discount > 0): ?>
            <h2 class="cost"><?= $model->price - ($model->price * ($model->discount / 100)) ?> UAH</h2>
            <h4 class="discount cost"><?= $model->price ?> UAH</h4>
        <?php else: ?>
            <h2 class="cost"><?= $model->price ?> <span>UAH</span></h2>
        <?php endif; ?>
        <div class="purchase">
            <?php $form = ActiveForm::begin() ?>
            <?= $form->field($purchaseForm, 'count', ['options' => ['class' => 'count']])->textInput(['type' => 'number', 'value' => 1, 'class' => 'count-input', 'pattern' => '[0-9]{1,2}'])->label(false) ?>
            <?php Modal::begin(['header' => Yii::t('app', 'Інформація для доставки'), 'toggleButton' => ['label' => Yii::t('app', 'Замовити'), 'class' => 'button']]) ?>
            <?= $form->field($purchaseForm, 'product', ['options' => ['class' => 'hidden']])->textInput(['value' => $model->name]) ?>
            <?= $form->field($purchaseForm, 'name')->textInput(['placeholder' => 'наприклад Боднаренко Назар Борисович']) ?>
            <?= $form->field($purchaseForm, 'phone') ?>
            <?= $form->field($purchaseForm, 'email') ?>
            <?= $form->field($purchaseForm, 'city') ?>
            <?= $form->field($purchaseForm, 'address') ?>
            <?php if ($model->category->url != 'decor' && $model->enableColor): ?>
                <?php $colors = array_flip($colors) ?>
                <?= $form->field($purchaseForm, 'color')->widget(\kartik\select2\Select2::className(), [
                    'data' => $colors,
                    'hideSearch' => true
                ]) ?>
            <?php else: ?>
                <?= $form->field($purchaseForm, 'color', ['options' => ['class' => 'hidden']])->textInput(['value' => '']) ?>
            <?php endif ?>
            <?= $form->field($purchaseForm, 'notes')->textarea(['placeholder' => Yii::t('app', 'Не обов\'язково')]) ?>
            <?= Html::submitButton(Yii::t('app', 'Відправити'), ['class' => 'submit']) ?>
            <?php Modal::end() ?>
            <?php if ($model->enableColor && $model->category->url != 'decor'): ?>
                <?php Modal::begin(['header' => Yii::t('app', 'Выберіть колір для дроту'), 'toggleButton' => ['label' => '<i class="fas fa-cart-plus"></i>', 'class' => 'button cart-sel'], 'options' => ['class' => 'color-modal']]) ?>
                <?= \kartik\select2\Select2::widget([
                    'name' => 'ccolor',
                    'data' => $colors,
                    'hideSearch' => true,
                    'options' => ['class' => 'cart-color']
                ]); ?>
                <?= Html::button(Yii::t('app', 'Добавити в корзину'), ['class' => 'btn cart-add']) ?>
                <?php Modal::end() ?>
            <?php else: ?>
                <?= Html::input('string', '', $colors[0], ['class' => 'cart-color hidden']) ?>
                <?= Html::button('<i class="fas fa-cart-plus"></i>', ['class' => 'button cart-add cart-sel']) ?>
            <?php endif ?>
            <?php ActiveForm::end() ?>
        </div>
        <div class="description">
            <div class="characteristic_">
                <?php if (count($model->characteristicName) > 0): ?>
                    <strong><?= Yii::t('app', 'Характеристики') ?>:</strong>
                    <?php foreach ($model->characteristicName as $k => $name): ?>
                        <?= '<div><div><span>' . $name . '</span></div><span>' . $model->characteristicText[$k] . '</span></div>' ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <?= $model->description ?>
        </div>
    </div>
</div>