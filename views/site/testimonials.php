<?php

use evgeniyrru\yii2slick\Slick;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Відгуки наших клієнтів');
$this->registerJs('$(document).on("mouseenter", ".testimonials .item", function () {
        var item = $(this)
        timer = setInterval(function () {
            item.find(".item-glr").slick("slickNext");
        },1200)
        $(this).mouseleave(function () {
            clearInterval(timer);
        })
    });
     $(document).ready(function(){
     $(".item-glr > div").not(".slick-list").not(":first-child").remove()
     $(window).resize(function(){if($(window).width() <= 670){
    $(".item-glr > div").not(".slick-list").not(":first-child").remove()}});
     $(".load").css("display", "none");$(".testimonials").removeClass("hide")})')
?>
<div class="headline text-center">
    <h3 class="headline-title"><?= Yii::t('app', 'Відгуки наших клієнтів') ?></h3>
</div>
<div class="load">
    <div class="lds-facebook">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<div class="testimonials hide">
    <?php foreach ($testimonials as $k => $testimonial): ?>
        <div class="item" id="<?= $testimonial->product->id ?>">
            <a class="product" href="/product/<?= $testimonial->product->url ?>">
                <?php if (count($testimonial->product->images) > 1): ?>
                    <?= Slick::widget([
                        'itemContainer' => 'div',
                        'containerOptions' => ['class' => 'item-glr', 'id' => 's' . $k],
                        'items' => $testimonial->product->images,
                        'clientOptions' => [
                            'slidesToShow' => 1,
                            'slidesToScroll' => 1,
                            'arrows' => false,
                            'adaptiveHeight' => true,
                            'dots' => false,
                            'responsive' => [
                                [
                                    'breakpoint' => 670,
                                    'settings' => 'unslick',
                                ],

                            ],
                        ],

                    ]); ?>
                <?php else: ?>
                <?=$testimonial->product->images[0]?>
                <?php endif; ?>
            </a>
            <div class="text">
          <span>
            <?= $testimonial->text ?>
          </span>
                <span class="date">(<?= strftime('%d %b. %Y ' . Yii::t('app', 'р.'), strtotime($testimonial->date)) ?>
                    )</span>
                <a href="<?= $testimonial->userurl ?>"
                   class="from"><?= $testimonial->username ?></a>
            </div>
        </div>
    <?php endforeach; ?>
</div>