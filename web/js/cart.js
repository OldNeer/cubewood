$(document).ready(function () {
    $('.cart-delete').click(function (e) {
        e.preventDefault();
        $.post('/cart-remove', {'id': $(this).attr('id')})
        $(this).parent().parent().remove();
        if ($('#cart_products tbody tr').length === 0) {
            $('.oe_cart').html('<div class="well well-lg">' + empty + '</div>');
        }
    });
    var addcan = true;
    $('.count-add').click(function (e) {
        e.preventDefault();
        if (addcan) {
            addcan = false;
            if ($(this).parent().find('.count').val() < 10) {
                $(this).parent().find('.count-error').css('display', 'none');
                var _this = this;
                $(_this).parent().find('.count').val(parseInt($(_this).parent().find('.count').val()) + 1);
                $('.totalprice').html((parseInt($('.totalprice').html()) + parseInt($(_this).parent().parent().parent().find('.price').html())).toFixed(2));
                $.post('/count-control', {
                    id: $(_this).parent().find('.count').attr('id'),
                    'function': '+'
                }).done(function () {
                    addcan = true
                })
            } else {
                $(this).parent().find('.count-error').css('display', 'block');
                var _this = this;
                setTimeout(function () {
                    $(_this).parent().find('.count-error').css('display', 'none');
                }, 2000)
                addcan = true
            }
        }
    })
    var minuscan = true;
    $('.count-minus').click(function (e) {
        e.preventDefault();
        if (minuscan) {
            minuscan = false;
            $(this).parent().find('.count-error').css('display', 'none');
            if ($(this).parent().find('.count').val() > 1) {
                $(this).parent().find('.count-error').css('display', 'none');
                var _this = this;
                $(_this).parent().find('.count').val(parseInt($(_this).parent().find('.count').val()) - 1);
                $('.totalprice').html(($('.totalprice').html() - $(_this).parent().parent().parent().find('.price').html()).toFixed(2));
                $.post('/count-control', {
                    id: $(_this).parent().find('.count').attr('id'),
                    'function': '-'
                }).done(function () {
                    minuscan = true;
                })
            } else {
                $(this).parent().find('.count-error').css('display', 'block');
                var _this = this;
                setTimeout(function () {
                    $(_this).parent().find('.count-error').css('display', 'none');
                }, 2000)
                minuscan = true;
            }
        }
    })
    $('.count').change(function () {
        $(this).parent().find('.count-error').css('display', 'none');
        if ($(this).val() > 10 || $(this).val() < 1) {
            $(this).parent().find('.count-error').css('display', 'block');
            var _this = this;
            setTimeout(function () {
                $(_this).parent().find('.count-error').css('display', 'none');
            }, 2000)
            return
        }
        var _this = this;
        $.post('/count-control', {
            id: $(_this).parent().find('.count').attr('id'),
            'function': '=',
            count: $(_this).parent().find('.count').val()
        }).done(function () {

        })
    });
    $(document).on('click', '.color:not(.selected)', function () {
        $('.selected').removeClass('selected');
        $(this).addClass('selected');
        $.post('/cart-change-color', {id: $(this).attr('data-id'), color: $(this).attr('data-color')});
    })
});