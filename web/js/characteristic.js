$(document).ready(function () {
    $(document).on('click', '.characteristic-button.add', function () {
        var row = $('.characteristic .main').clone();
        row.find('.characteristic-button').parent().remove();
        row.find('input').val('');
        row.append('<td><button type="button" class="btn btn-danger characteristic-button remove"><i class="fas fa-times"></i></button></td>')
        row.removeClass('main').appendTo('.characteristic tbody');
    });
    $(document).on('click', '.characteristic-button.remove', function () {
        $(this).parent().parent().remove();
    })
});