if(typeof allcolors === "undefined"){
    var allcolors = [];
}
var colors = "";
$(document).ready(function () {
    $('.products-form .btn-success').click(function (e) {
        e.preventDefault();
        create = false;
        $.each($('.color'), function (k, v) {
            colors = colors + $(v).data('color') + ";";
        });
        $('#products-colors').val(colors);
        $('.products-form form').submit();
    })
    $(".add-color button").click(function () {
        var color = $('.color-selector').val();
        if (!allcolors.includes(color)) {
            allcolors.push(color);
            $(".selected-colors").append("<div class='color' data-color='" + color + "' style='background: " + color + "'></div>");
            $(".color:last").click();
            $('.file-input').css('display', 'block');
        }
    })
    $(document).on('click', '.color:not(.color.can-close)', function () {
        $(".color").html("");
        $(".color").removeClass('can-close');
        $(this).html('<i class="fas fa-times"></i>');
        var color = $(this).data('color');
        if ($(this).hasClass('can-close')) {
            return;
        }
        $(this).addClass('can-close');
        $.ajax({
            method: "POST",
            url: '/admin/products/load-images',
            data: {folder: folder, color: color}
        }).done(function (data) {
            if (data !== "empty") {
                var initialPreview = [];
                var initialPreviewConfig = [];
                $.each(data.names, function (k, v) {
                    initialPreview.push('<img src="/' + v + '" class="file-preview-image">')
                    var name = v.split('/')[3]
                    initialPreviewConfig.push({
                        caption: name,
                        url: "/admin/products/image-delete",
                        extra: {
                            color: color,
                            name: name,
                            folder: folder
                        }
                    })
                });
                $('.imageSelect').fileinput('destroy');
                $('.imageSelect').fileinput({
                    autoOrientImage: true,
                    language: "en",
                    overwriteInitial: false,
                    purifyHtml: true,
                    trueresizeImage: false,
                    showRemove: true,
                    theme: "explorer",
                    uploadExtraData: {
                        color: color,
                        folder: folder,
                        colorId: $('.color.can-close').index()
                    },
                    initialPreview: initialPreview,
                    initialPreviewConfig: initialPreviewConfig,
                    uploadUrl: "/admin/products/image-upload"
                });
                $('.file-input').css('display', 'block');
            } else {
                $('.imageSelect').fileinput('destroy');
                $('.imageSelect').fileinput({
                    autoOrientImage: true,
                    language: "en",
                    overwriteInitial: false,
                    purifyHtml: true,
                    trueresizeImage: false,
                    showRemove: true,
                    theme: "explorer",
                    uploadExtraData: {
                        color: color,
                        folder: folder,
                        colorId: $('.color.can-close').index()
                    },
                    uploadUrl: "/admin/products/image-upload"
                });
            }
            $('.file-input').css('display', 'block');
        })
        $('.file-input').css('display', 'block');
    });
    $(document).on('click', '.can-close', function () {
        var num = $(this).index();
        var color = $(this).data('color');
        allcolors.splice(allcolors.indexOf(color), 1);
        $.ajax({
            method: "POST",
            url: '/admin/products/delete-color',
            data: {folder: folder, color: color}
        })
        $(this).remove();
        if ($(".color:eq(" + (num - 1) + ")").length) {
            $(".color:eq(" + (num - 1) + ")").click();
        } else if ($(".color:eq(" + (num + 1) + ")").length) {
            $(".color:eq(" + (num + 1) + ")").click();
        }
        if (!$('.selected-colors').children().length) {
            $('.file-input').css('display', 'none');
            $('.imageSelect').fileinput('clear')
        }
    });
    if (typeof create !== "undefined") {
        window.onbeforeunload = function (e) {
            $.ajax({
                method: "POST",
                url: "/admin/products/delete-all-images",
                data: {folder: folder, can: create}
            })
        }
    }
    $(".color:first").trigger("click");
    $('.imageSelect').on('filesorted', function (e, p) {
        var names = [];
        $.each(p.stack,function (k, v) {
            names.push(v.caption);
        });
        $.ajax({
            method: "POST",
            url: "/admin/products/sort",
            data: {
                folder: folder,
                secret: secret,
                color: $('.color.can-close').data('color'),
                names: names,
                colorId: $('.color.can-close').index()
            }
        }).done(function (data) {
            console.log(data);
            var newStack = p.stack;
            $.each(data, function (k, v) {
                newStack[k].caption = v;
                newStack[k].extra.name = v;
                $('.imageSelect').fileinput('updateStack', k, newStack[k]);
            })
        })
    });
});