$(document).ready(function () {
    var enabled = true;
    $(".open-menu").click(function (e) {
        e.preventDefault();
        open();
    });
    $(".mobile-nav .close").click(function () {
        close()
    })
    $(".eclipse").click(function () {
        close();
    })

    $('.nav-menu ul li a, .pjax li a').click(function (e) {
        if (typeof index !== "undefined") {
            e.preventDefault();
            if (enabled) {
                enabled = false;
                close();
                $('.wrap.content').css('display', 'none');
                $('.load').css('display', 'block');
                $.pjax.reload('#p0', {url: $(this).attr('href'), timeout: 5000}).done(function () {
                    $('.load').css('display', 'none');
                    $('.wrap.content').css('display', 'grid');
                    enabled = true;
                })
            }
        }
    });
    $('.search-button').click(function (e) {
        e.preventDefault();
        $('.search-form').addClass('fshow');
        $('.navbar-nav').fadeOut(300);
        if ($(window).width() <= 425) {
            $('.navbar-brand').fadeOut(300);
        }
    });
    $('.search-close').click(function () {
        $('.search-form').removeClass('fshow');
        if ($(window).width() > 991) {
            $('.navbar-nav').fadeIn(300);
        } else {
            $('.navbar-right').fadeIn(300);
        }
        if ($(window).width() <= 425) {
            $('.navbar-brand').fadeIn(300);
        }
    });
    if (typeof index !== "undefined") {
        $('.search-form').submit(function (e) {
                e.preventDefault();
                $('.wrap.content').css('display', 'none');
                $('.load').css('display', 'block');
                if ($('.search-form input').val() != "") {
                    $.pjax.reload('#p0', {
                        url: '/?search=' + $('.search-form input').val(),
                        timeout: 5000
                    }).done(function () {
                        $('.load').css('display', 'none');
                        $('.wrap.content').css('display', 'grid');
                    });
                } else {
                    $.pjax.reload('#p0', {
                        url: '/',
                        timeout: 5000
                    }).done(function () {
                        $('.load').css('display', 'none');
                        $('.wrap.content').css('display', 'grid');
                    });
                }
                $('.search-close').click();
            }
        )
    }
    $('.footer-subscribe').submit(function (e) {
        e.preventDefault();
        var email = $(this).find('input').val();
        $.ajax({
            url: '/subscribe',
            type: "POST",
            data: {email: email}
        }).done(function (data) {
            if(data.type === "success"){
                $('.subscribe-modal').removeClass('error');
                $('.subscribe-modal').removeClass('success');
                $('.subscribe-modal').addClass('success');
                $('.subscribe-modal-body').text(data.message);
                $('.subscribe-modal').modal('show');
            } else {
                $('.subscribe-modal').removeClass('error');
                $('.subscribe-modal').removeClass('success');
                $('.subscribe-modal').addClass('error');
                $('.subscribe-modal-body').text(data.message);
                $('.subscribe-modal').modal('show');
            }
        });
    })
});

function open() {
    $(".eclipse").fadeIn(500);
    $("html").addClass('hide-scrollbar');
    $(".mobile-nav").css('width', '337px');
    $('.mobile-nav .close').css('display', 'block');
}

function close() {
    $('.mobile-nav .close').css('display', 'none');
    $(".mobile-nav").css('width', '0');
    $(".eclipse").fadeOut(500);
    $("html").removeClass('hide-scrollbar');
}