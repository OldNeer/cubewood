$(document).ready(function () {
    $(document).on('click', '.glr-main', function () {
        $(".glr-main").slick("slickNext")
    });

    var timeout;
    $(document).on('mouseleave', '.glr-main', function () {
        timeout = setTimeout(function () {
            $(".glr-main").slick("slickGoTo", 0);
        }, 2000)
    })
    $(document).on('mouseenter', '.glr-main', function () {
        clearTimeout(timeout);
    })

    $(document).on('click', '.color:not(.color.selected)', function () {
        $(".color").html("");
        $(".color").removeClass('selected');
        $(this).html('<i class="fas fa-times"></i>');
        $(this).addClass('selected');
        var color = $(this).data('color');
        $("#p0").html('<div class="lds-facebook"><div></div><div></div><div></div></div>');
        $.pjax.reload('#p0', {
            type: 'POST',
            timeout: 5000,
            data: {
                color: color
            }
        })
    })
    $(document).on('click', '.selected', function () {
        $(this).html("");
        $(this).removeClass('selected');
        $("#p0").html('<div class="lds-facebook"><div></div><div></div><div></div></div>');
        $.pjax.reload('#p0', {
            type: 'POST',
            timeout: 5000
        })
    })
    $('.select2-selection').ready(function () {
        if (typeof defColor !== "undefined") {
            $('.select2-selection').css('background-color', defColor);
            $('.selection > span').css('color', defColor);
            $(document).on('click', '.select2-selection', function () {
                $.each($('.select2-results__option'), function (k, v) {
                    var color = $(v).attr('id');
                    color = color.split('-')[5];
                    if (typeof color === "undefined") {
                        color = $(v).attr('id');
                        color = color.split('-')[4];
                    }
                    $(v).css('background-color', color);
                    $(v).css('color', color);
                })
            })
        }
    })
    $('#purchaseform-color, .cart-color').change(function () {
        $('.select2-selection').css('background-color', $(this).val());
        $('.selection > span').css('color', $(this).val());
    })
    if (typeof defColor !== "undefined") {
        if (del) {
            $('.cart-sel').addClass('cart-add');
            $('.cart-sel').attr('data-target', '');
            $('.cart-sel').html('<i class="fas fa-times"></i>');
            $('.cart-sel').attr('title', deltitle);
        } else {
            $('.cart-sel').removeClass('cart-add');
            $('.cart-sel').attr('data-target', '#' + $('.color-modal').attr('id'));
            $('.cart-sel').html('<i class="fas fa-cart-plus"></i>');
            $('.cart-sel').attr('title', addtitle);
        }
    }
    $(document).on('click', '.cart-add', function () {
        $('.color-modal').modal('hide');
        if (!del) {
            $.post('/cart-add', {
                'id': id,
                'color': $('.cart-color').val(),
                'count': $('#purchaseform-count').val()
            }).done(function () {
                del = true
            });
            $('.cart-sel').html('<i class="fas fa-times"></i>');
            $('.cart-sel').attr('title', deltitle);
            if (typeof defColor !== "undefined") {
                $('.cart-sel').addClass('cart-add');
                $('.cart-sel').attr('data-target', '');
            }
            if ($('.cart_line').length == 0) {
                totalPrice = totalPrice + price;
                $(".cart-content").empty();
                $(".cart-content").html('<div class="items"></div><div class=\'text-center\'><span class=\'h5\'><div id=\'cart_total\'><div class=\'row\' id=\'order_total\'><span class=\'col-xs-6 text-right h4\'>' + vsogo + ':</span><span class=\'col-xs-6 text-left h4\' style=\'white-space: nowrap;\'>' + totalPrice + ' ' + valuta + '</span></div></div></span><a class=\'btn btn-primary\' href=\'/cart\'>' + pereglanuty + ' (<span class=\'totalCount\'>1</span> ' + st + ')</a></div>')
                $(".cart-content .items").html($(".cart-content .items").html() + "<div id='" + id + "' class='row cart_line'><div class='col-xs-3'><span><img class='img img-responsive img-rounded' src='" + $(image).attr('src') + "'></span></div><div class='col-xs-9'><a href='/product/" + url + "'><span class='h6'>" + name + "</span></a><span class='text-muted'></span>" + countText + ": <span class='count'>" + $('#purchaseform-count').val() + "</span><span class='text-muted'>" + price + " " + valuta + "</span></div></div>");
            } else {
                $(".cart-content .items").html($(".cart-content .items").html() + "<div id='" + id + "' class='row cart_line'><div class='col-xs-3'><span><img class='img img-responsive img-rounded' src='" + $(image).attr('src') + "'></span></div><div class='col-xs-9'><a href='/product/" + url + "'><span class='h6'>" + name + "</span></a><span class='text-muted'></span>" + countText + ": <span class='count'>" + $('#purchaseform-count').val() + "</span><span class='text-muted'>" + price + " " + valuta + "</span></div></div>");
                totalPrice = totalPrice + price * $('#purchaseform-count').val();
                $('.text-left.h4').html((totalPrice) + ' ' + valuta);
                $('.totalCount').html($('.cart_line').length);
            }
        } else {
            $.post('/cart-remove', {'id': id}).done(function () {
                del = false
            });
            $('.cart-sel').html('<i class="fas fa-cart-plus"></i>');
            $('.cart-sel').attr('title', addtitle);
            if (typeof defColor !== "undefined") {
                $('.cart-sel').removeClass('cart-add');
                $('.cart-sel').attr('data-target', '#' + $('.color-modal').attr('id'));
            }
            $(".cart-content .items #" + id).remove();
            if ($('.cart_line').length == 0) {
                $('.cart-content').html('<div class="well well-lg">' + empyCart + '</div>');
            } else {
                totalPrice = totalPrice - price * $('#' + id).find('.count').text();
                $('.text-left.h4').html((totalPrice) + ' ' + valuta);
                $('.totalCount').html($('.cart_line').length);
            }
        }
    })
});