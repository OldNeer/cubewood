$(document).ready(function () {
    $('.save').click(function () {
        var ids = $('.item').map(function () {
            return $(this).attr('id');
        }).get();
        $.post('/admin/sort/do', {ids: ids}).done(function (data) {
            window.location = '/admin/';
        })
        $(this).remove();
    })
})