$(document).ready(function () {
    var done = false
    if ($(window).width() > 945) {
        $.each($('.response .item'), function (k, v) {
            cut($(v).find('img').height(), $(v).find('.text a span:eq(0)'));
        })
    } else done = true;
    $(window).resize(function () {
        if ($(window).width() > 945) {
            $.each($('.response .item'), function (k, v) {
                var fulltext = $(v).find('.full-text').text().trim();
                $(v).find('.text a span:eq(0)').text(fulltext);
                cut($(v).find('img').height(), $(v).find('.text a span:eq(0)'));
            })
            done = false;
        } else if(done == false){
            $.each($('.response .item'), function (k, v) {
                var fulltext = $(v).find('.full-text').text().trim();
                $(v).find('.text a span:eq(0)').text(fulltext);
            })
            done = true;
        }

    });
});

function cut(height, block) {
    var fulltext = $(block).text().trim();
    var slicedtext = fulltext;
    while ($(block).height() > height) {
        slicedtext = $(block).text().trim();
        $(block).text(slicedtext.slice(0, -1));
    }
    if (slicedtext.length == 1) {
        $(block).text(fulltext);
    }
    if (fulltext !== slicedtext) {
        $(block).text($(block).text() + "...");
    }
}