tinymce.init({
    selector: '.tinymce',
    language: 'uk-UA',
    plugins: [
        'advlist autolink link lists charmap preview hr anchor pagebreak spellchecker',
        'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking',
        'save table contextmenu directionality emoticons template textcolor'
    ],
    toolbar: 'fullscreen | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | preview | forecolor backcolor emoticons',
    mobile: {
        theme: 'mobile'
    }
})